-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ftdb
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_pipes_abstractpipe`
--

DROP TABLE IF EXISTS `transaction_pipes_abstractpipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_pipes_abstractpipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `sender_account` varchar(28) NOT NULL,
  `recipient_account` varchar(28) NOT NULL,
  `description` varchar(300) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_pipes_ab_user_id_3a5e0f3d_fk_accounts_` (`user_id`),
  CONSTRAINT `transaction_pipes_ab_user_id_3a5e0f3d_fk_accounts_` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_pipes_abstractpipe`
--

LOCK TABLES `transaction_pipes_abstractpipe` WRITE;
/*!40000 ALTER TABLE `transaction_pipes_abstractpipe` DISABLE KEYS */;
INSERT INTO `transaction_pipes_abstractpipe` VALUES (1,'test_cp1','543165454384354','54364146543687638','test no1','2018-06-18 10:12:27.788705',2,0),(2,'test_cp2','57654367357','5435436873587','Test no.2','2018-06-18 10:12:54.298130',3,0),(3,'test_sp1','54398738769879','87654687368766','test no.3','2018-06-18 10:13:43.739718',3,0),(4,'test_sp2','573548435865433','9264365436843','test no.4','2018-06-18 10:14:53.879539',2,0),(6,'test_sp4','154324646546','324354316846136584','Test no.7','2018-06-23 13:15:11.824053',4,0),(8,'test_sp5','70984646543','54365436543','Test no.8','2018-06-23 14:50:45.968890',4,0),(9,'test_cp3','18002793554312365795214521','18002793554312365795214522','Test no.9','2018-06-24 12:09:11.099561',1,0),(10,'First child','18002793554312365795214521','543643684','Remember about your parents. Study hard.','2018-06-24 16:49:13.886506',1,0),(11,'Second child','18002793554312365795214522','543643684368454','Remember about your parents. Study hard.','2018-06-24 16:49:46.863219',1,0),(12,'Internet UPC','18002793554312365795214525','644638463843643','This pipe is due to charge you monthly for internet connection.','2018-06-24 16:53:16.444524',1,0),(13,'Energy fee','18002793554312365795214521','54364536843654','Test energy pipe','2018-06-24 16:54:30.542211',1,0);
/*!40000 ALTER TABLE `transaction_pipes_abstractpipe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-24 21:23:36
