-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ftdb
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-06-17 21:27:18.550171'),(2,'contenttypes','0002_remove_content_type_name','2018-06-17 21:27:18.650457'),(3,'auth','0001_initial','2018-06-17 21:27:18.966920'),(4,'auth','0002_alter_permission_name_max_length','2018-06-17 21:27:18.982578'),(5,'auth','0003_alter_user_email_max_length','2018-06-17 21:27:18.989083'),(6,'auth','0004_alter_user_username_opts','2018-06-17 21:27:18.989083'),(7,'auth','0005_alter_user_last_login_null','2018-06-17 21:27:19.004709'),(8,'auth','0006_require_contenttypes_0002','2018-06-17 21:27:19.004709'),(9,'auth','0007_alter_validators_add_error_messages','2018-06-17 21:27:19.020361'),(10,'auth','0008_alter_user_username_max_length','2018-06-17 21:27:19.020361'),(11,'auth','0009_alter_user_last_name_max_length','2018-06-17 21:27:19.035987'),(12,'accounts','0001_initial','2018-06-17 21:27:19.405798'),(13,'admin','0001_initial','2018-06-17 21:27:19.590676'),(14,'admin','0002_logentry_remove_auto_add','2018-06-17 21:27:19.590676'),(15,'sessions','0001_initial','2018-06-17 21:27:19.653199'),(16,'transaction_pipes','0001_initial','2018-06-18 10:09:59.465575'),(17,'transaction_pipes','0002_auto_20180624_1926','2018-06-24 17:28:42.659532');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-24 21:23:35
