from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator

# Create your models here.


'''
    Our user class which is extension of AbstractUser class.
'''


class User(AbstractUser):
    phone_regex = RegexValidator(regex=r'^\+\d{9,15}$',
                                 message="A phone number must be entered in the format: '+48999999999'. "
                                         "Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=False, default='+48')

    REQUIRED_FIELDS = ['first_name', 'last_name', 'email', 'phone_number']
