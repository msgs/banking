from django.contrib import admin
from .models import User

# Register your models here.


# User model and corresponding admin view


class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
