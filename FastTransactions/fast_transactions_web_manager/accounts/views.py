from django.shortcuts import render
from .forms import MyUserCreationForm
from django.shortcuts import redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import UsernameField
from django import forms


# Create your views here.

@login_required
def profile(request):
    args = {'user': request.user}
    return render(request, 'accounts/profile.html', args)


def registration(request):
    if request.method == 'POST':
        form = MyUserCreationForm(request.POST)
        if form.is_valid():
            # save user data
            form.save()

            # log in new user
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)

            return redirect('/account/profile/')
    else:
        form = MyUserCreationForm()
    args = {'form': form}
    return render(request, 'accounts/registration.html', args)


class MyLoginView(LoginView):
    LoginView.form_class.username = UsernameField(max_length=254, widget=forms.TextInput(
        attrs={'autofocus': True, 'class': 'form-control', 'aria-describedby': 'username-addon'})
    )

    LoginView.form_class.password = forms.CharField(label="Password", strip=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'aria-describedby': 'password-addon'})
    )
