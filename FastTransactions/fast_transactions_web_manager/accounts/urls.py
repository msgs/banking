from django.urls import re_path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'accounts'

urlpatterns = [
    re_path(r'^login/$', views.MyLoginView.as_view(template_name="accounts/login.html")),
    re_path(r'^registration/$', views.registration),
    re_path(r'^profile/$', views.profile),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(template_name="accounts/logout.html"))
]
