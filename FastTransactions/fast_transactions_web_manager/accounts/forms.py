from django.contrib.auth import forms
from django import forms as d_forms
from .models import User
from django.contrib.auth import password_validation

'''
    My custom user creation form to register new users
'''


class MyUserCreationForm(forms.UserCreationForm):
    email = d_forms.EmailField(required=True, widget=d_forms.EmailInput(
        attrs={'class': 'form-control', 'aria-describedby': 'email-addon'}))
    first_name = d_forms.CharField(max_length=30, required=True, widget=d_forms.TextInput(
        attrs={'class': 'form-control', 'aria-describedby': 'first-name-addon'}))
    last_name = d_forms.CharField(max_length=150, required=True, widget=d_forms.TextInput(
        attrs={'class': 'form-control', 'aria-describedby': 'last-name-addon'}))
    phone_number = d_forms.CharField(max_length=17, initial='+48', required=True, widget=d_forms.TextInput(
        attrs={'class': 'form-control', 'aria-describedby': 'first-name-addon'}))
    forms.UserCreationForm.password1 = d_forms.CharField(label="Password", strip=False, widget=d_forms.PasswordInput(
        attrs={'class': 'form-control', 'aria-describedby': 'password1-addon'}),
                                                         help_text=password_validation.password_validators_help_text_html())
    forms.UserCreationForm.password2 = d_forms.CharField(required=True, label="Password confirmation", widget=d_forms.PasswordInput(
        attrs={'class': 'form-control', 'aria-describedby': 'password2-addon'}), strip=False,
                                  help_text="Enter the same password as before, for verification.")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self._meta.model.USERNAME_FIELD in self.fields:
            self.fields[self._meta.model.USERNAME_FIELD].widget.attrs.update(
                {'autofocus': True, 'class': 'form-control', 'aria-describedby': 'username-addon'})

    class Meta(forms.UserCreationForm.Meta):
        model = User
        fields = forms.UserCreationForm.Meta.fields + ('first_name', 'last_name', 'email', 'phone_number',)

    def get_errors(self):
        my_errors = []
        for field in self.fields:
            if field in self.errors:
                my_errors += [self.errors[field][0]]
        return my_errors
