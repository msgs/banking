# Generated by Django 2.0.5 on 2018-06-24 17:26

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import transaction_pipes.validators


class Migration(migrations.Migration):

    dependencies = [
        ('transaction_pipes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoryEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('description', models.CharField(max_length=300)),
            ],
        ),
        migrations.RemoveField(
            model_name='transactionrequest',
            name='approved',
        ),
        migrations.AddField(
            model_name='abstractpipe',
            name='approved',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='abstractpipe',
            name='sender_account',
            field=models.CharField(max_length=28, validators=[django.core.validators.RegexValidator(message='An account number must have 9-24 digits.', regex='^\\d{9,28}$'), transaction_pipes.validators.sender_account_validator]),
        ),
        migrations.AlterField(
            model_name='schedulepipe',
            name='amount',
            field=models.FloatField(validators=[transaction_pipes.validators.amount_validator]),
        ),
        migrations.AlterField(
            model_name='schedulepipe',
            name='date',
            field=models.IntegerField(validators=[transaction_pipes.validators.date_validator]),
        ),
        migrations.AlterField(
            model_name='transactionrequest',
            name='amount',
            field=models.FloatField(validators=[transaction_pipes.validators.amount_validator]),
        ),
        migrations.AlterField(
            model_name='transactionrequest',
            name='date',
            field=models.DateField(),
        ),
        migrations.AddField(
            model_name='historyevent',
            name='pipe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='transaction_pipes.AbstractPipe'),
        ),
    ]
