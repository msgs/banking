from django.db import models
from django.core.validators import RegexValidator
from accounts.models import User
from . import validators

# Create your models here.


'''
    Abstract model of transaction pipe in our service.
'''


class AbstractPipe(models.Model):
    account_regex = RegexValidator(regex=r'^\d{9,28}$', message="An account number must have 9-24 digits.")

    name = models.CharField(max_length=30, blank=False)
    sender_account = models.CharField(validators=[account_regex, validators.sender_account_validator], max_length=28,
                                      blank=False)
    recipient_account = models.CharField(validators=[account_regex], max_length=28, blank=False)
    description = models.CharField(max_length=300, blank=False)
    creation_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    approved = models.BooleanField(default=False)

    REQUIRED_FIELDS = ['name', 'sender_account', 'recipient_account', 'description', 'user']

    # Define how the pipe will be shown in our system
    def __str__(self):
        return self.name


'''
    Type of transaction pipe in our service. The time of transfer is scheduled by the date parameter.
    0 - everyday
    1-28
    32 - every last day of month
'''


class SchedulePipe(AbstractPipe):
    date = models.IntegerField(blank=False, validators=[validators.date_validator])
    amount = models.FloatField(blank=False, validators=[validators.amount_validator])

    REQUIRED_FIELDS = AbstractPipe.REQUIRED_FIELDS + ['date', 'amount']

    '''
    Function which returns readable version of date field.
    '''

    def get_date2str(self):
        if self.date == 0:
            response = 'every day'
        else:
            response = 'every ' + str(self.date)
            if self.date in [1, 21]:
                response += 'st'
            elif self.date in [2, 22]:
                response += 'nd'
            elif self.date in [3, 23]:
                response += 'rd'
            else:
                response += 'th'
            response += ' day'
        return response


'''
    Type of transaction pipe in our service. The time of transfer is not defined. It depends on user request.
'''


class ConventionalPipe(AbstractPipe):
    pass


'''
    Request class. It is connected with ConventionalPipe. 
    User generates request when he/she wants to make a transfer request.
'''


class TransactionRequest(models.Model):
    date = models.DateField(blank=False)
    amount = models.FloatField(blank=False, validators=[validators.amount_validator])
    pipe = models.ForeignKey(ConventionalPipe, on_delete=models.CASCADE, blank=False)

    REQUIRED_FIELDS = ['date', 'amount', 'pipe']


'''
    History event class. It is connected with Abstract Pipe and child classes. 
    When request of conventional pipe's or schedule pipe's request is handling history event is created.
'''


class HistoryEvent(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=300, blank=False)
    pipe = models.ForeignKey(AbstractPipe, on_delete=models.CASCADE, blank=False)

    REQUIRED_FIELDS = ['date', 'description', 'pipe']


'''
Helper function that returns pipe object by given id or None if no pipe has been found.
'''


def get_pipe_by_id(pipe_id):
    pipe = ConventionalPipe.objects.all().filter(id=pipe_id)
    if not pipe:
        pipe = SchedulePipe.objects.all().filter(id=pipe_id)
    if not pipe:
        pipe = None
    else:
        pipe = pipe[0]
    return pipe
