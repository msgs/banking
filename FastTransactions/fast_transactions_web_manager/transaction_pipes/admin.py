from django.contrib import admin
from.models import ConventionalPipe, SchedulePipe, TransactionRequest

# Register your models here.

my_models = [ConventionalPipe, SchedulePipe, TransactionRequest]
admin.site.register(my_models)
