from django.core.exceptions import ValidationError
import re

'''
Validator for date field in Schedule transaction pipe.
Value is valid if value is between 0-28 or is equal 32.
'''


def date_validator(value):
    message = 'Ensure this value of day of month is between 0 - 28 or is equal to 32.'
    code = 'wrong_date_value'
    values = [0, 28, 32]
    invalid_input = True

    if (values[0] <= value <= values[1]) or values[2] == value:
        invalid_input = False
    if invalid_input:
        raise ValidationError(message, code=code)


'''
Validator for amount field.
Value is valid if value has 2 points of precision and has positive value.
'''


def amount_validator(value):
    message1 = 'Ensure this value of amount has 2 points of precision.'
    message2 = 'Ensure that value of amount is positive number.'
    code = 'wrong_value'
    invalid_input = True

    float_value = float(value)
    str_float_value = str(float_value)
    str_float_value_array = str_float_value.rsplit('.')
    if len(str_float_value_array[-1]) <= 2 and len(str_float_value_array) == 2:
        invalid_input = False
    if invalid_input:
        raise ValidationError(message1, code=code)
    if float_value <= 0:
        raise ValidationError(message2, code=code)


'''
Validator for sender account number.
Value is valid if number is in supported accounts list.
'''


def sender_account_validator(value):
    message = 'We currently support only limited number of banks. ' \
              'Supported ones are listed in Transaction Pipes tab'
    code = 'wrong_value'
    invalid_input = True
    # Supported accounts - prefixes
    supported_accounts = [
        '180027935543\d{14}'  # Ti Banking: 180027935543 + 14 numbers
    ]

    if re.match(r"(?=(" + '|'.join(supported_accounts) + r"))", value) is not None:
        invalid_input = False
    if invalid_input:
        raise ValidationError(message, code=code)
