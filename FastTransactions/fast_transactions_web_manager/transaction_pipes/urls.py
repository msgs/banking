from django.urls import re_path
from . import views

app_name = 'transaction_pipes'

urlpatterns = [
    re_path(r'^your-pipes/', views.TPOverview.as_view(template_name='transaction_pipes/overview.html'),
            name="overview"),
    re_path(r'^pipe-creation/$', views.tp_creation_view),
    re_path(r'^(?P<id>\d+)/history/$', views.TPHistory.as_view(template_name='transaction_pipes/history.html'),
            name='history'),
    re_path(r'^(?P<id>\d+)/modify/$', views.TPHistory.as_view(template_name='transaction_pipes/modify.html'),
            name='modify'),
    re_path(r'^(?P<id>\d+)/pending-requests/$',
            views.TPPendingRequests.as_view(template_name='transaction_pipes/pending_requests.html'),
            name='pending_requests'),
    re_path(r'^(?P<pipe_id>\d+)/request-creation/$', views.tp_request_creation_view, name='new_request'),
]
