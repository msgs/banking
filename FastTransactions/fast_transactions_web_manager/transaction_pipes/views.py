from django.shortcuts import render, redirect, HttpResponseRedirect
from django.views.generic import FormView, TemplateView
from django.urls import reverse
from django.http import Http404
from . import forms
from . import models

# Create your views here.

'''
Transaction Pipe creation function view.
'''


def tp_creation_view(request):
    if request.method == 'POST':
        abstract_form = forms.AbstractTPCreationForm(request.POST)
        schedule_form = forms.ScheduleTPCreationForm(request.POST)
        if abstract_form.is_valid():

            # Create schedule transaction pipe
            if abstract_form.cleaned_data['pipe_type'] == '2':
                if schedule_form.is_valid():
                    schedule_pipe = models.SchedulePipe()
                    schedule_pipe.name = abstract_form.cleaned_data['name']
                    schedule_pipe.sender_account = abstract_form.cleaned_data['sender_account']
                    schedule_pipe.recipient_account = abstract_form.cleaned_data['recipient_account']
                    schedule_pipe.description = abstract_form.cleaned_data['description']
                    schedule_pipe.date = schedule_form.cleaned_data['date']
                    schedule_pipe.amount = schedule_form.cleaned_data['amount']
                    schedule_pipe.user = request.user
                    schedule_pipe.save()
                    return redirect('/tp/my-pipes/')

            # Create conventional transaction pipe
            else:
                conventional_pipe = models.ConventionalPipe()
                conventional_pipe.name = abstract_form.cleaned_data['name']
                conventional_pipe.sender_account = abstract_form.cleaned_data['sender_account']
                conventional_pipe.recipient_account = abstract_form.cleaned_data['recipient_account']
                conventional_pipe.description = abstract_form.cleaned_data['description']
                conventional_pipe.user = request.user
                conventional_pipe.save()
                return redirect('/tp/your-pipes/')

        my_errors = []
        for field in abstract_form.fields:
            if field in abstract_form.errors:
                my_errors += [field.replace('_', ' ').upper() + ': ' + abstract_form.errors[field][0]]
        if abstract_form.cleaned_data['pipe_type'] == '2':
            for field in schedule_form.fields:
                if field in schedule_form.errors:
                    my_errors += [field.replace('_', ' ').upper() + ': ' + schedule_form.errors[field][0]]
    else:
        abstract_form = forms.AbstractTPCreationForm()
        schedule_form = forms.ScheduleTPCreationForm()
        my_errors = None
    args = {'abstract_form': abstract_form, 'schedule_form': schedule_form, 'my_errors': my_errors}
    return render(request, 'transaction_pipes/create.html', args)


'''
Transaction Pipes overview class view.
'''


class TPOverview(FormView):
    # Get method
    def get(self, request, *args, **kwargs):
        conventional_pipes = models.ConventionalPipe.objects.all().filter(user_id=request.user.id)
        schedule_pipes = models.SchedulePipe.objects.all().filter(user_id=request.user.id)
        my_args = {'conventional_pipes': conventional_pipes, 'schedule_pipes': schedule_pipes}
        return render(request, self.template_name, my_args)

    # Post method
    def post(self, request, *args, **kwargs):
        request_message = request.POST.get('type').rsplit('.')
        type_of_request = request_message[0]
        pipe_id = request_message[-1]

        # Redirect to a proper page
        if type_of_request == 'history':
            return HttpResponseRedirect(reverse('transaction_pipes:history', args=(pipe_id,)))
        elif type_of_request == 'modify':
            return HttpResponseRedirect(reverse('transaction_pipes:modify', args=(pipe_id,)))
        elif type_of_request == 'new_request':
            return redirect('transaction_pipes:new_request', pipe_id=pipe_id)
        elif type_of_request == 'pending_requests':
            return HttpResponseRedirect(reverse('transaction_pipes:pending_requests', args=(pipe_id,)))
        else:
            raise Http404


'''
TP history class view. Shows history of an pipe.
'''


class TPHistory(TemplateView):

    # Get method
    def get(self, request, *args, **kwargs):
        pipe_id = self.kwargs['id']
        user_id = request.user.id

        # Get pipe data
        pipe = models.get_pipe_by_id(pipe_id)
        if pipe is None or pipe.user.id is not user_id:
            raise Http404

        # Get pipe's history data
        history_events = models.HistoryEvent.objects.all().filter(pipe_id=pipe_id)

        my_args = {'pipe': pipe, 'history_events': history_events}
        return render(request, self.template_name, my_args)


'''
TP modify class view. Shows a form where user can modify pipe content.
'''


class TPModify(FormView):

    # Get method
    def get(self, request, *args, **kwargs):
        pipe_id = self.kwargs['id']
        user_id = request.user.id

        # Get pipe data
        pipe = models.get_pipe_by_id(pipe_id)
        if pipe is None or pipe.user.id is not user_id:
            raise Http404

        my_args = {'pipe': pipe}
        return render(request, self.template_name, my_args)

    # Post method
    def post(self, request, *args, **kwargs):
        request_message = request.POST.get('type').rsplit('.')
        type_of_request = request_message[0]
        pipe_id = request_message[-1]

        # Redirect to a proper page
        if type_of_request == 'save':
            # TODO - save changes
            return HttpResponseRedirect(reverse('transaction_pipes:overview'))
        elif type_of_request == 'reject':
            return HttpResponseRedirect(reverse('transaction_pipes:request', args=(pipe_id,)))
        elif type_of_request == 'delete':
            # TODO - delete pipe
            return HttpResponseRedirect(reverse('transaction_pipes:overview'))
        else:
            raise Http404


'''
TP Pending requests class view. Shows pending requests of an pipe.
'''


class TPPendingRequests(FormView):

    # Get method
    def get(self, request, *args, **kwargs):
        pipe_id = self.kwargs['id']
        user_id = request.user.id

        # Get pipe data
        pipe = models.get_pipe_by_id(pipe_id)
        if pipe is None or pipe.user.id is not user_id or type(pipe) is not models.ConventionalPipe:
            raise Http404

        # Get pipe's pending requests
        pending_requests = models.TransactionRequest.objects.all().filter(pipe_id=pipe_id)

        my_args = {'pipe': pipe, 'pending_requests': pending_requests}
        return render(request, self.template_name, my_args)

    # Post method
    def post(self, request, *args, **kwargs):
        request_message = request.POST.get('type').rsplit('.')
        type_of_request = request_message[0]
        pipe_id = request_message[1]
        request_id = request_message[2]

        # Redirect to a proper page
        if type_of_request == 'delete':
            tp_request = models.TransactionRequest.objects.all().filter(pipe_id=pipe_id).filter(id=request_id)
            if tp_request:
                tp_request.delete()
            return HttpResponseRedirect(reverse('transaction_pipes:overview'))
        else:
            raise Http404


'''
Transaction Pipes request creation function view.
'''


def tp_request_creation_view(request, pipe_id):
    if request.method == 'POST':
        form = forms.TPRequestCreationForm(request.POST)
        if form.is_valid():

            # Create tp request
            if form.is_valid():
                conventional_pipe = models.get_pipe_by_id(pipe_id)
                if conventional_pipe is None or type(conventional_pipe) is not models.ConventionalPipe:
                    raise Http404

                tp_request = models.TransactionRequest()
                tp_request.date = form.cleaned_data['date']
                tp_request.amount = form.cleaned_data['amount']
                tp_request.pipe = conventional_pipe
                tp_request.save()
                return redirect('/tp/your-pipes/')

        my_errors = []
        for field in form.fields:
            if field in form.errors:
                my_errors += [field.replace('_', ' ').upper() + ': ' + form.errors[field][0]]

    # Get method
    else:
        form = forms.TPRequestCreationForm()
        my_errors = None
    args = {'form': form, 'my_errors': my_errors}
    return render(request, 'transaction_pipes/create_request.html', args)
