from django.apps import AppConfig


class TransactionPipesConfig(AppConfig):
    name = 'transaction_pipes'
