from django import forms
from . import models

'''
Abstract Transaction Pipe creation form.
'''


class AbstractTPCreationForm(forms.ModelForm):
    name = forms.CharField(max_length=30, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'aria-describedby': 'name-addon'}))
    sender_account = forms.CharField(max_length=28, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'aria-describedby': 'sender-addon'}))
    recipient_account = forms.CharField(max_length=28, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'aria-describedby': 'recipient-addon'}))
    description = forms.CharField(max_length=300, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'aria-describedby': 'description-addon'}))
    pipe_type = forms.ChoiceField(choices=[(1, 'Conventional'), (2, 'Schedule')], initial=1, widget=forms.Select(
        attrs={'class': 'form-control', 'aria-describedby': 'type-addon', 'id': 'type-choice',
               'onchange': 'checkType();'}))

    class Meta:
        model = models.AbstractPipe
        fields = ('name', 'sender_account', 'recipient_account', 'description',)


'''
Conventional Transaction Pipe creation form.
'''

# Future usage possible
# class ConventionalTPCreationForm(forms.ModelForm):
#     ...
#     class Meta:
#         model = models.ConventionalPipe
#         fields = ...


'''
Schedule Transaction Pipe creation form.
'''


class ScheduleTPCreationForm(AbstractTPCreationForm):
    data_choices = [(-1, 'last'), (0, 'every')]
    for i in range(1, 29):
        data_choices += [(i, str(i))]
    date = forms.ChoiceField(choices=data_choices, widget=forms.Select(
        attrs={'class': 'form-control', 'aria-describedby': 'date-addon'}))
    amount = forms.FloatField(required=True, widget=forms.NumberInput(
        attrs={'class': 'form-control', 'aria-describedby': 'amount-addon', 'step': 0.01}))

    class Meta:
        model = models.SchedulePipe
        fields = ('date', 'amount',)


'''
Transaction Request creation form.
'''


class TPRequestCreationForm(forms.ModelForm):
    date = forms.DateField(required=True, widget=forms.DateInput(
        attrs={'class': 'form-control', 'aria-describedby': 'date-addon', 'autofocus': 'autofocus',
               'placeholder': 'YYYY-MM-DD'}))
    amount = forms.FloatField(required=True, widget=forms.NumberInput(
        attrs={'class': 'form-control', 'aria-describedby': 'amount-addon', 'step': 0.01}))

    class Meta:
        model = models.TransactionRequest
        fields = ('date', 'amount',)
