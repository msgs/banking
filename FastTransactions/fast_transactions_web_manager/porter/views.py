from django.shortcuts import render

# Create your views here.


def home(request):
    return render(request, 'porter/homepage.html')


def transaction_pipes(request):
    return render(request, 'porter/transaction_pipes.html')


def individual(request):
    return render(request, 'porter/individual.html')


def company(request):
    return render(request, 'porter/company.html')


def about_us(request):
    return render(request, 'porter/about_us.html')

