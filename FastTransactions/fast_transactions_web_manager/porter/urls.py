from django.urls import re_path
from . import views

app_name = 'porter'

urlpatterns = [
    re_path(r'^$', views.home),
    re_path(r'^transaction-pipes/$', views.transaction_pipes),
    re_path(r'^individual/$', views.individual),
    re_path(r'^company/$', views.company),
    re_path(r'^about-us/$', views.about_us),
]
