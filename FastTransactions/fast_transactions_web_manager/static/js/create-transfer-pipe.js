/*
This function check the type of creating pipe on the page.
It ensure that proper fields are shown.
 */
function checkType() {
    if (document.getElementById('type-choice').value == 2) {
        document.getElementById('create-schedule-pipe').style.display = 'block';
        document.getElementById('id_amount').value = '';
    } else {
        document.getElementById('create-schedule-pipe').style.display = 'none';
        document.getElementById('id_amount').value = '0';
    }
}