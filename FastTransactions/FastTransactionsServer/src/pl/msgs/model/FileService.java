package pl.msgs.model;

import java.io.*;

/**
 * this class is used to saving objects in files
 */


public abstract class FileService {


    //Saving data to file
    public static boolean saveToFile(Object o, String path) {
        try {
            File file = new File(path);
            if (!file.exists()){
                if(file.getParentFile() != null){
                    file.getParentFile().mkdirs(); // create parents directories
                }
                file.createNewFile();
            }

            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file, false));

            out.writeObject(o);

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;

        }
        return true;

    }

    //Loading one object from file
    public static Object loadFromFile(String path){
        Object o = null;
        try{
            File file = new File(path);
            if(file.exists()){
                ObjectInput in = new ObjectInputStream(new FileInputStream(file));
                o = in.readObject();
                in.close();
            }
        }catch (IOException e){
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        return o;
    }

    //Removing file
    public static void removeFile(String path){
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }

}
