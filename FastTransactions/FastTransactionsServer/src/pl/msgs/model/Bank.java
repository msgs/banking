package pl.msgs.model;

/**
 * This class is Bank description
 *
 * It used to identify bank in Ft system
 */
public class Bank {

    private String name;
    private String identifier;

    //constructor
    public Bank(String identifier) {
        this.identifier = identifier;
        this.name = "UNKNOWN";
    }

    public Bank(String name, String identifier) {
        this.name = name;
        this.identifier = identifier;
    }

    //getters
    public String getName() {
        return name;
    }

    public String getIdentifier() {
        return identifier;
    }

    //setters
    public void setName(String name) {
        this.name = name;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
