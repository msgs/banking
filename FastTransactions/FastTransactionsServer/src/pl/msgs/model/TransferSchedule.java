package pl.msgs.model;

import java.io.Serializable;

/**
 * Transfers which are waiting fo realization under schedule.
 *
 *  this is equivalent of table in database
 */


public class TransferSchedule implements Serializable {

    private int pipeId;
    private int date;
    private double amount;


    //constructor
    public TransferSchedule(int pipeId, int date, double amount) {
        this.pipeId = pipeId;
        this.date = date;
        this.amount = amount;
    }

    public TransferSchedule(){

    }

    //getters and setters
    public int getPipeId() {
        return pipeId;
    }

    public void setPipeId(int pipeId) {
        this.pipeId = pipeId;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

}
