package pl.msgs.model;

import java.io.Serializable;

/**
 * This is main class which define schema of FT orders.
 *
 *  this is equivalent of table in database
 */
public class Transfer implements Serializable {

    //general information
    private String date;
    private String description;
    private int userId;
    private int id;
    private boolean approved;

    private String name;
    private String payerAccountNo;
    private String recipientAccountNo;

    //Constructor
    public Transfer(String date, String description, String name, String payerAccountNo, String recipientAccountNo, int userId, int id, boolean approved) {
        this.date = date;
        this.description = description;
        this.name = name;
        this.payerAccountNo = payerAccountNo;
        this.recipientAccountNo = recipientAccountNo;
        this.userId = userId;
        this.id = id;
        this.approved = approved;
    }

    public Transfer(){

    }

    //getters

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getPayerAccountNo() {
        return payerAccountNo;
    }

    public String getRecipientAccountNo() {
        return recipientAccountNo;
    }

    public int getUserId() {
        return userId;
    }

    public int getId() {
        return id;
    }

    //setters

    public void setDate(String date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPayerAccountNo(String payerAccountNo) {
        this.payerAccountNo = payerAccountNo;
    }

    public void setRecipientAccountNo(String recipientAccountNo) {
        this.recipientAccountNo = recipientAccountNo;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
