package pl.msgs.model;

/**
 * Transfer which was realized
 *
 * this is equivalent of table in database
 */


public class HistoryTransfer  {

    private int id;
    private String date;
    private String description;
    private int pipeId;

    //constructors
    public HistoryTransfer(int id, String date, String description, int pipeId) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.pipeId = pipeId;
    }

    public HistoryTransfer() {
    }

    //getters and setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPipeId() {
        return pipeId;
    }

    public void setPipeId(int pipeId) {
        this.pipeId = pipeId;
    }
}
