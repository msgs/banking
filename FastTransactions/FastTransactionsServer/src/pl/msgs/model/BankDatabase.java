package pl.msgs.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This only to serialize a ArrayList<Bank>
 *
 * It will be very useful if system will be supports more then one server
 * and more then one bank.
 *
 *
 */


public class BankDatabase extends ArrayList<Bank> implements Serializable{


}
