package pl.msgs.model;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * Transfer confirmation
 *
 * If state is true, transfer was realized and confirmed.
 * If state is false, transfer was rejected.
 */
public class Confirmation implements Serializable {

    private boolean state;
    private LocalDateTime localDateTime;
    private ReadyTransfer readyTransfer;

    //constructor
    public Confirmation(boolean state, LocalDateTime localDateTime, ReadyTransfer readyTransfer) {
        this.state = state;
        this.localDateTime = localDateTime;
        this.readyTransfer = readyTransfer;
    }

    // getters and setters
    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public ReadyTransfer getReadyTransfer() {
        return readyTransfer;
    }

    public void setReadyTransfer(ReadyTransfer readyTransfer) {
        this.readyTransfer = readyTransfer;
    }
}
