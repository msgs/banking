package pl.msgs.model;

import java.io.Serializable;

/**
 * Transfers which are waiting fo realization.
 *
 *  this is equivalent of table in database
 */

public class TransferRequest implements Serializable {

    private double amount;
    private String date;
    private int pipeId;
    private int id;

    //constructor
    public TransferRequest(double amount, String date, int pipeId, int id) {
        this.amount = amount;
        this.date = date;
        this.pipeId = pipeId;
        this.id = id;
    }

    public TransferRequest(){

    }


    //getters and setters
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPipeId() {
        return pipeId;
    }

    public void setPipeId(int pipeId) {
        this.pipeId = pipeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
