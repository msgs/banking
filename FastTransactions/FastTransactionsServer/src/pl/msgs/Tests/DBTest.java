package pl.msgs.Tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDate;

/**
 * connection with database test
 */


public class DBTest {

    public static void main(String[] args){
        String jdbcUrl = "jdbc:mysql://localhost:3306/ftdb?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String user = "root";
        String password = "root";
        LocalDate date = LocalDate.now();
        try{
            Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
            System.out.println("Connection successful!" + date.toString());
        }catch (Exception e){
            e.printStackTrace();
        }


    }

}
