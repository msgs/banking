package pl.msgs.server;


import pl.msgs.model.ListTodayOrders;
import pl.msgs.model.ReadyTransfer;

import java.io.*;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.nio.charset.StandardCharsets;

/**
 * Class which used to handling Bank connection
 */


public class HandlingBank implements Runnable {

    private volatile boolean running = true;
    private Thread thread;
    private Socket socket;
    private BankConnection bankConnection;
    private static final String READY = "ready";
    private static final String ERROR = "error";
    private static final String SENDING = "sending";
    private static final String CONFIRM = "confirm";
    private static final String END = "end";
    private static final int SEND = 1;
    private static final int RECEIVE = 2;

    //Creating connection handler

    public HandlingBank(Socket socket, BankConnection bankConenction) {
        this.socket = socket;
        this.bankConnection = bankConenction;
    }

    public void startConnection(){
        thread = new Thread(this);
        thread.start();
    }

    public void closeConnection(){
        running = false;

    }

    public void run(){
        try(
            InputStream inStream = socket.getInputStream();
            OutputStream outStream = socket.getOutputStream();
        ){
            Scanner in = new Scanner(inStream, "UTF-16");
            PrintWriter out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);

            String input, output;

            //This section is used to swap information with banks

            input = in.nextLine();
            if (input.equals(READY)){
                ListTodayOrders todayOrders = Server.prepareTransfers();
                if (todayOrders.size() > 0) {
                    for (int i = 0; i < todayOrders.size(); i++) {
                        output = todayOrders.get(i).toString();
                        out.println(output);
                        input = in.nextLine();
                        if (!input.equals(CONFIRM)){
                            output = ERROR;
                            out.println(output);
                            break;
                        }
                    }
                    output = CONFIRM;
                    out.println(output);
                }else{
                    output = ERROR;
                    out.println(output);
                }
            }
            input = in.nextLine();
            while (!input.equals(CONFIRM) && !input.equals(ERROR)){
                ListTodayOrders tempList = Server.getListConfirmations();
                String[] temp = input.split("/");
                Boolean approved;
                if (temp[7].equals("true")){
                    approved =true;
                }else{
                    approved = false;
                }
                ReadyTransfer tempTransfer = new ReadyTransfer(temp[0], temp[1], temp[2], temp[3],temp[4],Integer.parseInt(temp[5]),Integer.parseInt(temp[6]),approved,Double.parseDouble(temp[8]));
                tempList.add(tempTransfer);
                Server.setListConfirmations(tempList);
                output = CONFIRM;
                out.println(output);
                input=in.nextLine();
            }
            output = END;
            out.println(output);
            //in.nextLine();
            //output = END;
            //out.println(output);
            in.close();
            out.close();

            //bankConnection.closeBankConnection();

        }catch(NoSuchElementException e){
            //bankConnection.closeBankConnection();
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
