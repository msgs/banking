package pl.msgs.server;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * This is class which answer for connection server witch AdminApp
 */


public class AdminConnection implements Runnable {

    private volatile boolean running = true;
    private volatile boolean reading = true;
    private Thread thread;
    private final int PORT = 9898;
    private Logger logger = Logger.getLogger(AdminConnection.class.getName());

    /**
     * Starts the server connection.
     */
    public void startConnection() {
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try (
                // Only local connections. One connection at the same time.
                ServerSocket serverSocket = new ServerSocket(PORT, 0, InetAddress.getByName(null));
        ) {
            while (running) {
                Socket user = serverSocket.accept();
                InputStream inStream = user.getInputStream();
                OutputStream outStream = user.getOutputStream();

                logger.warning("User is locally connected to the server");

                // Main working section - processing user commands.
                Scanner userInput = new Scanner(inStream, "UTF-16");
                PrintWriter out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);

                out.println("Connection with server is established.");
                out.println("Available commands: \"exit\";\"stop\".");

                String input, output;
                AdminTalkProtocol adminTalkProtocol = new AdminTalkProtocol(this);



                reading = true;
                while (reading) {
                    input = userInput.nextLine();
                    output = adminTalkProtocol.processInput(input);
                    out.println(output);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.warning("Thread is off.");
    }

    /**
     * After calling this function this thread will be stopped soon.
     */
    public void stop() {
        reading = false;
        running = false;
        Server.stopServer();
    }

    /**
     * After calling this function the server will stop the current connection soon.
     */
    public void stopReading() {
        reading = false;
    }
}
