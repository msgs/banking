package pl.msgs.server;


import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Application to manage Ft server
 */


public class AdminApp {

    private static final String END = "Goodbye";

    private static String host = "localhost";
    private static int port = 9898;
    private static Socket socket;
    private static InputStream inStream;
    private static OutputStream outStream;
    private static Scanner in, userInput;
    private static PrintWriter out;
    private static Logger logger = Logger.getLogger(AdminApp.class.getName());

    //in main is communication protocol
    //changing info with server
    public static void main(String args[]){
        try{
            System.out.println("Welcome Administrator!");
            socket = new Socket(host, port);
            inStream = socket.getInputStream();
            outStream = socket.getOutputStream();
            in = new Scanner(inStream, "UTF-16");
            out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);
            userInput = new Scanner(System.in);

            String input, output;
            input = in.nextLine();
            System.out.println(input);
            input = in.nextLine();
            System.out.println(input);

            do{
                System.out.print(">");
                output = userInput.nextLine();
                System.out.println("Echo: " + output);
                out.println(output);
                input = in.nextLine();
                System.out.println(input);
            }while (!input.equalsIgnoreCase(END));

        }catch (IOException e){
            logger.severe("Cannot connect to the server!");
            e.printStackTrace();
        }
    }
}
