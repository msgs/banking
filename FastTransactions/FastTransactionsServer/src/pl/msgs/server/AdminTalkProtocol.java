package pl.msgs.server;

import java.util.logging.Logger;

/**
 * Communication protocol between server and admin app
 */


public class AdminTalkProtocol {

    private AdminConnection adminConnection;
    private static final String EXIT = "exit";
    private static final String STOP = "stop";

    private Logger logger = Logger.getLogger(AdminTalkProtocol.class.getName());

    public AdminTalkProtocol(AdminConnection adminConnection) {
        this.adminConnection = adminConnection;
    }



    public String processInput(String input) {
        String output = "";


        switch(input){
            // End of server's work.
            case EXIT:
                adminConnection.stopReading();
                output = "Goodbye";
                break;
            // End of server's work.
            case STOP:
                adminConnection.stop();
                output = "Goodbye";
                break;
            default:
                output = "Error: Unknown command \"" + input + "\"";
                logger.warning("Unrecognized command from serverApp: \"" + input + "\"");

        }


        return output;
    }

}
