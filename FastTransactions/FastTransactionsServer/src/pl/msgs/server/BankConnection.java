package pl.msgs.server;

import javax.net.ssl.SSLServerSocketFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 *  This class is used to made connection with banking systems
 */
public class BankConnection implements Runnable {

    private volatile boolean running = true;
    private Thread thread;
    private final int port = 7777;
    private Logger logger = Logger.getLogger(BankConnection.class.getName());




    public void startBankConnection(){
        thread = new Thread(this);
        thread.start();
    }

    //public void closeBankConnection(){
    //    running = false;
    //}

    public void run(){
        try {
            ServerSocket serverSocket = new ServerSocket(port);//((SSLServerSocketFactory)SSLServerSocketFactory.getDefault()).createServerSocket(port); //
            while(running){
                Socket incoming = serverSocket.accept();

                logger.info("Connection with bank has been made.");
                HandlingBank handlingBank = new HandlingBank(incoming, this);
                handlingBank.startConnection();
            }
            logger.info("End of bank connection thread.");

        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
