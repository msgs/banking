package pl.msgs.server;

import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.msgs.model.*;
import org.hibernate.Session;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.logging.Logger;

/**
 * Main fast transactions system class.
 */

public class Server {

    private static String time;

    private static LocalDate today;
    private static LocalTime localTime;

    //private static volatile BankDatabase bankDatabase;
    private static Logger logger = Logger.getLogger(Server.class.getName());
    private static volatile boolean running = true;

    private static Session session;
    private static ListTodayOrders listConfirmations = new ListTodayOrders();
    private static Transaction tx;




    public static void main(String[] args){
        logger.info("Server is starting...");

        //Time when server started
        localTime = LocalTime.now();
        today = LocalDate.now();
        time = "Time: " + localTime + " Date: " + today.getDayOfMonth() + "." + today.getMonth() + "." + today.getYear();

        // Server application started.
        //String pathBanks = "./banks.txt";

        //bankDatabase = (BankDatabase) FileService.loadFromFile(pathBanks);

        session = HibernateUtil.getSessionFactory().getCurrentSession();


        logger.info("Online services are turning on...");
        BankConnection bankConnection = new BankConnection();
        bankConnection.startBankConnection();
        logger.info("Online services are running.");

        logger.info("Local services are turning on...");
        AdminConnection adminConnection = new AdminConnection();
        adminConnection.startConnection();

        logger.info("Local services are running.");

        /*
        ArrayList<ReadyTransfer> todayOrders = prepareTransfers();
        System.out.println(todayOrders.get(0).getAmount());
        System.out.println(todayOrders.get(0).getDescription());
        System.out.println(todayOrders.get(1).getAmount());
        System.out.println(todayOrders.get(1).getDescription());
        */

        logger.info("System is ready!");
        while (running){
            try{
                Thread.sleep(500);
            }catch (InterruptedException e){
                logger.warning(e.toString());
            }
        }

        //close session
        HibernateUtil.closeSessionFactory();
        try {
            Thread.sleep(5000);
        }catch (InterruptedException e){
            logger.warning(e.toString());
        }

        System.exit(0);

        //save BankDataBase
        //FileService.removeFile(pathBanks);
        //savedBanks = FileService.saveToFile(bankDatabase, pathBanks);

         //if (!savedBanks){
         //   logger.warning("Saved bank database was impossible!");
         //}
    }


    /**
     * Prepares transfers which should be realized that day
     *
     * @return ArrayList<TodayTransfer>
     */
    public static ListTodayOrders prepareTransfers(){
        LocalDate localDate = LocalDate.now();


        session = HibernateUtil.getSessionFactory().getCurrentSession();
        tx = session.beginTransaction();
        Query schedule = session.createQuery("select transferSchedule from TransferSchedule transferSchedule where date=" + localDate.getDayOfMonth());
        Query everyDay = session.createQuery("select transferSchedule from TransferSchedule transferSchedule where date=" + 0 );
        Query reguest = session.createQuery("select transferRequest from TransferRequest transferRequest where date=" + localDate.toString());
        Query lastDay = session.createQuery("select transferSchedule from TransferSchedule transferSchedule where date=" + 32);

        List <TransferSchedule>transferSchedules = schedule.list();
        List <TransferRequest> transferRequests = reguest.list();
        List <TransferSchedule> everyDayTransfers = everyDay.list();
        ListTodayOrders todayOrders = new ListTodayOrders();
        //System.out.println(transferSchedules.get(0).getAmount());
        for (TransferSchedule ts : transferSchedules){
           todayOrders.add(new ReadyTransfer((Transfer) session.load(Transfer.class, ts.getPipeId()), ts.getAmount()));

        }
        for (TransferSchedule ed : everyDayTransfers){
           todayOrders.add(new ReadyTransfer((Transfer) session.load(Transfer.class, ed.getPipeId()), ed.getAmount()));
        }

        if (localDate.lengthOfMonth() == localDate.getDayOfMonth()) {
            List<TransferSchedule> lastDayTransfers = lastDay.list();
            for (TransferSchedule ld : lastDayTransfers){
                todayOrders.add(new ReadyTransfer((Transfer) session.load(Transfer.class, ld.getPipeId()), ld.getAmount()));
            }

        }

        for (TransferRequest tr : transferRequests){
            todayOrders.add(new ReadyTransfer((Transfer) session.load(Transfer.class, tr.getPipeId()), tr.getAmount()));
        }
        tx.commit();

        return todayOrders;
    }

    //getters and setters
    public static ListTodayOrders getListConfirmations() {
        return listConfirmations;
    }

    public static void setListConfirmations(ListTodayOrders listConfirmations) {
        Server.listConfirmations = listConfirmations;
        if (Server.listConfirmations.size() >0) {
            updateDatabase();
        }
    }

    public static void stopServer(){
        running = false;
    }


    /**
     * Saving realized orders in database
     */
    private static void updateDatabase(){

        for (int i = 0; i < listConfirmations.size(); i++){
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
                Query query = session.createQuery("from HistoryTransfer order by id DESC");
                query.setMaxResults(1);
                HistoryTransfer last = (HistoryTransfer) query.uniqueResult();
            int id;
            if (last != null){
                    id = last.getId();
                    id++;
                }else {
                id = 1;
            }
                session.saveOrUpdate(new HistoryTransfer(id, LocalDate.now().toString(), "true", listConfirmations.get(i).getId() ));
            tx.commit();
            session.close();
        }



    }

}
