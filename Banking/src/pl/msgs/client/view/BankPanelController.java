package pl.msgs.client.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import pl.msgs.FTConnection.ListTodayOrders;
import pl.msgs.FTConnection.ReadyTransfer;
import pl.msgs.client.ClientApp;
import pl.msgs.client.view.FTPipes.FTPipeOverviewController;
import pl.msgs.client.view.accounts.AccountOverviewController;
import pl.msgs.model.Account;
import pl.msgs.model.CreditAccount;
import pl.msgs.model.HistoryEvent;
import pl.msgs.model.Placing;

import java.io.IOException;
import java.util.ArrayList;

public class BankPanelController {
    @FXML
    private Label clientFullName;
    @FXML
    private ScrollPane mainPane;

    // reference to the client application.
    private ClientApp clientApp;


    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public BankPanelController() {
    }

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Handles Profile button in bank panel.
     */
    @FXML
    private void handleProfile() {
        refreshData();
        VBox vBox = new VBox();
        vBox.getChildren().clear();

        Label titleLabel = new Label("Profile");
        titleLabel.setFont(Font.font(14));
        titleLabel.setPadding(new Insets(10, 10, 0 ,10));
        vBox.getChildren().add(titleLabel);
        Separator titleSeparator = new Separator(Orientation.HORIZONTAL);
        titleLabel.setPadding(new Insets(10, 0, 10, 0));
        vBox.getChildren().add(titleSeparator);

        vBox.prefWidthProperty().bind(mainPane.widthProperty().subtract(30));
        mainPane.setContent(vBox);
        mainPane.setVvalue(0);
    }

    /**
     * Handles Settings button in bank panel.
     */
    @FXML
    private void handleSettings() {
        refreshData();
        VBox vBox = new VBox();
        vBox.getChildren().clear();

        Label titleLabel = new Label("Settings");
        titleLabel.setFont(Font.font(14));
        titleLabel.setPadding(new Insets(10, 10, 0 ,10));
        vBox.getChildren().add(titleLabel);
        Separator titleSeparator = new Separator(Orientation.HORIZONTAL);
        titleLabel.setPadding(new Insets(10, 0, 10, 0));
        vBox.getChildren().add(titleSeparator);

        vBox.prefWidthProperty().bind(mainPane.widthProperty().subtract(30));
        mainPane.setContent(vBox);
        mainPane.setVvalue(0);
    }

    /**
     * Handles Accounts button in bank panel.
     */
    @FXML
    public void handleAccounts() {
        refreshData();
        showAccounts(clientApp.getClient().getAccounts());
    }

    /**
     * Logout a client.
     */
    @FXML
    private void handleLogout() {
        clientApp.getClientConnection().logout();
        clientApp.showLoginPanel();
    }

    /**
     * Shows all necessary data.
     */
    public void showClientData() {
        clientFullName.setText(clientApp.getClient().getFirstName() + " " + clientApp.getClient().getLastName());
        showAccounts(clientApp.getClient().getAccounts());
    }

    /**
     * Is called by the client application to give a reference back to itself.
     *
     * @param clientApp
     */
    public void setClientApp(ClientApp clientApp) {
        this.clientApp = clientApp;
    }

    /**
     * Shows overview of client accounts.
     * @param accounts arraylist of accounts.
     */
    private void showAccounts(ArrayList<Account> accounts) {
        VBox vBox = new VBox();
        vBox.getChildren().clear();
        if(accounts != null) {
            for (int i = 0; i < accounts.size(); i++) {
                try {
                    // Loads the account overview.
                    FXMLLoader loader = new FXMLLoader();
                    if (accounts.get(i) instanceof CreditAccount) {
                        loader.setLocation(ClientApp.class.getResource("view/accounts/CreditAccount.fxml"));
                    } else if (accounts.get(i) instanceof Placing) {
                        loader.setLocation(ClientApp.class.getResource("view/accounts/PlacingAccount.fxml"));
                    } else {
                        loader.setLocation(ClientApp.class.getResource("view/accounts/StandardAccount.fxml"));
                    }
                    GridPane accountOverview = (GridPane) loader.load();

                    // Give the controller required data.
                    AccountOverviewController controller = loader.getController();
                    controller.showAccountOverview(accounts.get(i));
                    controller.setParentController(this);
                    controller.setClientApp(clientApp);

                    // Add new account box to the account container.
                    vBox.getChildren().add(accountOverview);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        vBox.prefWidthProperty().bind(mainPane.widthProperty().subtract(30));
        mainPane.setContent(vBox);
        mainPane.setVvalue(0);
    }

    /**
     * Shows overview of client account history.
     * @param accountHistory arraylist of account history.
     */
    public void showAccountHistory(String accountNumber, ArrayList<HistoryEvent> accountHistory) {
        VBox vBox = new VBox();
        vBox.getChildren().clear();

        Label titleLabel = new Label("History of an account: " + accountNumber);
        titleLabel.setFont(Font.font(14));
        titleLabel.setPadding(new Insets(10, 10, 0 ,10));
        vBox.getChildren().add(titleLabel);
        Separator titleSeparator = new Separator(Orientation.HORIZONTAL);
        titleLabel.setPadding(new Insets(10, 0, 10, 0));
        vBox.getChildren().add(titleSeparator);

        if(accountHistory != null) {
            for (int i = 0; i < accountHistory.size(); i++) {
                try {
                    // Loads the account overview.
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(ClientApp.class.getResource("view/HistoryOverview.fxml"));

                    GridPane accountOverview = (GridPane) loader.load();

                    // Give the controller required data.
                    HistoryOverviewController controller = loader.getController();
                    controller.showHistoryOverview(accountHistory.get(i));

                    // Add new account history event box to the account history container.
                    vBox.getChildren().add(accountOverview);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        vBox.prefWidthProperty().bind(mainPane.widthProperty().subtract(30));
        mainPane.setContent(vBox);
        mainPane.setVvalue(0);
    }

    /**
     * Shows transfer pane.
     * @param account
     */
    public void showTransferPane(Account account) {
        VBox vBox = new VBox();
        vBox.getChildren().clear();
        try {
            // Loads the account overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClientApp.class.getResource("view/TransferPane.fxml"));
            GridPane transferPane = (GridPane) loader.load();

            // Give the controller required data.
            TransferPaneController controller = loader.getController();
            controller.showTransferPane(account, clientApp);

            // Show transfer pane
            vBox.getChildren().add(transferPane);
        } catch (IOException e) {
            e.printStackTrace();
        }
        vBox.prefWidthProperty().bind(mainPane.widthProperty().subtract(30));
        mainPane.setContent(vBox);
        mainPane.setVvalue(0);
    }


    /**
     * Shows overview of client pipes.
     * @param pipes arraylist of pipes.
     */
    public void showPipes(String accountNumber, ListTodayOrders pipes) {
        VBox vBox = new VBox();
        vBox.getChildren().clear();

        Label titleLabel = new Label("Pipes of an account: " + accountNumber);
        titleLabel.setFont(Font.font(14));
        titleLabel.setPadding(new Insets(10, 10, 0 ,10));
        vBox.getChildren().add(titleLabel);
        Separator titleSeparator = new Separator(Orientation.HORIZONTAL);
        titleLabel.setPadding(new Insets(10, 0, 10, 0));
        vBox.getChildren().add(titleSeparator);

        if(pipes != null) {
            for (int i = 0; i < pipes.size(); i++) {
                try {
                    // Loads the pipe overview.
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(ClientApp.class.getResource("view/FTPipes/FTPipeOverview.fxml"));

                    GridPane accountOverview = (GridPane) loader.load();

                    // Give the controller required data.
                    FTPipeOverviewController controller = loader.getController();
                    controller.showPipeOverview(pipes.get(i));
                    controller.setParentController(this);
                    controller.setClientApp(clientApp);

                    // Add new pipe box to the pipe container.
                    vBox.getChildren().add(accountOverview);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        vBox.prefWidthProperty().bind(mainPane.widthProperty().subtract(30));
        mainPane.setContent(vBox);
        mainPane.setVvalue(0);
    }



    private void refreshData() {
        try {
            clientApp.getClientConnection().refreshData();
        } catch (IOException | ClassNotFoundException e) {
            clientApp.showRefreshError();
        }
    }
}
