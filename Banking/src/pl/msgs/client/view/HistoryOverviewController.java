package pl.msgs.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import pl.msgs.model.HistoryEvent;
import pl.msgs.util.DateToString;

public class HistoryOverviewController {
    @FXML
    private Label typeLabel;
    @FXML
    private Label dateLabel;
    @FXML
    private Label valueLabel;
    @FXML
    private Label commentLabel;
    @FXML
    private Circle circle;

    public void showHistoryOverview(HistoryEvent event) {
        String eventType;
        if (event.getType().equals("card")) {
            eventType = "Card payment";
            circle.setFill(Color.web("0077FF"));
        } else if (event.getType().equals("transfer")) {
            eventType = "Bank transfer";
            circle.setFill(Color.web("3ad100"));
        } else {
            eventType = "Balance change";
            circle.setFill(Color.web("ff8e00"));
        }
        typeLabel.setText(eventType);
        dateLabel.setText(DateToString.format(event.getDate()));
        valueLabel.setText(String.format("%.2f PLN", event.getValue()));
        commentLabel.setText(event.getComment());
    }
}
