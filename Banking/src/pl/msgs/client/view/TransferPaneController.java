package pl.msgs.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import pl.msgs.client.ClientApp;
import pl.msgs.model.Account;

public class TransferPaneController {
    @FXML
    private Label accountNumberLabel;
    @FXML
    private TextField destinationTextField;
    @FXML
    private TextField valueTextField;
    @FXML
    private TextField titleTextField;
    @FXML
    private TextArea commentTextArea;

    private ClientApp clientApp;

    public void showTransferPane(Account account, ClientApp clientApp) {
        accountNumberLabel.setText(account.getAccountNumber());
        this.clientApp = clientApp;
    }

    @FXML
    private void handleTransfer() {
        if (accountNumberLabel.getText() != null && accountNumberLabel.getText().length() != 0 ) {
            if (destinationTextField.getText() != null && destinationTextField.getText().length() == 26 && destinationTextField.getText().matches("[0-9]+") ) {
                if (valueTextField.getText() != null && valueTextField.getText().length() != 0) {
                    if (titleTextField.getText() != null && titleTextField.getText().length() != 0) {
                        try {
                            Double.parseDouble(valueTextField.getText());

                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("[");
                            stringBuilder.append(accountNumberLabel.getText());
                            stringBuilder.append(";;;;");
                            stringBuilder.append(destinationTextField.getText());
                            stringBuilder.append(";;;;");
                            stringBuilder.append(valueTextField.getText());
                            stringBuilder.append(";;;;");
                            stringBuilder.append(titleTextField.getText().replaceAll("\\[", "(").replaceAll("]", ")").replaceAll(";;;;", ","));
                            stringBuilder.append(";;;;");
                            if (commentTextArea.getText() == null || commentTextArea.getText().length() == 0)
                                commentTextArea.setText("No additional comment");
                            stringBuilder.append(commentTextArea.getText().replaceAll("\\[", "(").replaceAll("]", ")").replaceAll(";;;;", ","));
                            stringBuilder.append("]");

                            int answer = clientApp.getClientConnection().makeTransfer(stringBuilder.toString());

                            if (answer == 0) {
                                showError("Wrong sending account...", "");
                            } else if (answer == 1) {
                                showInfo("Transfer successfully ended", "");
                            } else if (answer == 2) {
                                showError("Wrong value", "You cannot transfer 0");
                            } else if (answer == 3) {
                                showError("Wrong value", "Not enough money to send");
                            } else if (answer == 4) {
                                clientApp.showConnectionError();
                            }
                        } catch (NumberFormatException e) {
                            showError("Wrong value", "Value have to be a number");
                        }
                    }
                }
            } else {
                showError("Wrong destination account number", "It should contain 26 numbers.");
            }
        }
    }

    /**
     * Generates alert.
     */
    private void showError(String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(clientApp.getPrimaryStage());
        alert.setTitle("Banking application error");
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }

    /**
     * Generates info.
     */
    private void showInfo(String header, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(clientApp.getPrimaryStage());
        alert.setTitle("Banking application");
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.setOnCloseRequest(event -> {
            clientApp.showAccount();
            alert.close();
        });

        alert.showAndWait();
    }
}

