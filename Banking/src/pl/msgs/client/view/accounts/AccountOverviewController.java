package pl.msgs.client.view.accounts;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import pl.msgs.client.ClientApp;
import pl.msgs.client.view.BankPanelController;
import pl.msgs.model.Account;
import pl.msgs.model.CreditAccount;
import pl.msgs.model.Placing;

import java.io.IOException;

public class AccountOverviewController {
    // All accounts type.
    @FXML
    private Label accountTypeLabel;
    @FXML
    private Label accountNumberLabel;
    @FXML
    private Label balanceLabel;
    @FXML
    private Label costLabel;
    @FXML
    private Label cardsQuantityLabel;
    @FXML
    private Circle circle;
    @FXML
    private Button cardButton;
    @FXML
    private Button pipesButton;

    // Placing account
    @FXML
    private Label rateLabel;

    // Credit account
    @FXML
    private Label creditLabel;
    @FXML
    private Label creditRateLabel;

    private Account account;
    private BankPanelController bankPanelController;
    private ClientApp clientApp;


    public void showAccountOverview(Account account) {
        this.account = account;

        accountNumberLabel.setText(account.getAccountNumber());
        balanceLabel.setText(String.format("%.2f PLN", account.getBalance()));
        costLabel.setText(String.format("%.2f PLN", account.getCost()));
        cardsQuantityLabel.setText(Integer.toString(account.getCards().size()));
        if(account.getCards().size() == 0) {
            cardButton.setVisible(false);
        }
        if(account.getWaitingTransactions().size() == 0) {
            pipesButton.setVisible(false);
        }
        if (account instanceof CreditAccount) {
            accountTypeLabel.setText("Credit Account");
            creditLabel.setText(String.format("%.2f PLN", ((CreditAccount) account).getCredit()));
            creditRateLabel.setText(String.format("%.2f %%", ((CreditAccount) account).getBankRate()));
            circle.setFill(Color.web("ff8e00"));
        } else if (account instanceof Placing) {
            accountTypeLabel.setText("Saving Account");
            rateLabel.setText(String.format("%.2f %%", ((Placing) account).getBankRate()));
            circle.setFill(Color.web("3ad100"));
        } else {
            accountTypeLabel.setText("Standard Account");
            circle.setFill(Color.web("0077FF"));
        }
    }

    /**
     * Is called by the bankPanelController to give a reference back to itself.
     *
     * @param bankPanelController
     */
    public void setParentController(BankPanelController bankPanelController) {
        this.bankPanelController = bankPanelController;
    }

    /**
     * Is called by the client application to give a reference back to itself.
     *
     * @param clientApp
     */
    public void setClientApp(ClientApp clientApp) {
        this.clientApp = clientApp;
    }

    @FXML
    private void handleTransfer() {
        bankPanelController.showTransferPane(account);
    }

    @FXML
    private void handleHistory() {
        try {
            bankPanelController.showAccountHistory(account.getAccountNumber(), clientApp.getClientConnection().getAccountHistory(account));
        } catch (IOException | ClassNotFoundException e) {
            clientApp.showRefreshError();
        }
    }

    @FXML
    private void handlePipes() {
        try {
            clientApp.getClientConnection().refreshData();
            bankPanelController.showPipes(account.getAccountNumber(), account.getWaitingTransactions());
        } catch (IOException | ClassNotFoundException e) {
            clientApp.showRefreshError();
        }
    }

    @FXML
    private void handlePayOff() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Banking Application");
        alert.setHeaderText("Right now you cannot pay off your credit.");
        alert.setContentText("Dear customer\nThis functionality will be implemented in the following update.\nUntil then cost related with your credit will not be incurred\nHave a great day.");

        alert.showAndWait();
    }
}