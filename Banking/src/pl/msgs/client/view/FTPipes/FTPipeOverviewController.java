package pl.msgs.client.view.FTPipes;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import pl.msgs.FTConnection.ReadyTransfer;
import pl.msgs.client.ClientApp;
import pl.msgs.client.view.BankPanelController;

public class FTPipeOverviewController {
    // All accounts type.
    @FXML
    private Label pipeNameLabel;
    @FXML
    private Label recipientAccountNumberLabel;
    @FXML
    private Label amountLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label approvedLabel;
    @FXML
    private Circle circle;
    @FXML
    private Button approveButton;

    private ReadyTransfer pipe;
    private BankPanelController bankPanelController;
    private ClientApp clientApp;


    public void showPipeOverview(ReadyTransfer pipe) {
        this.pipe = pipe;

        pipeNameLabel.setText(pipe.getName());
        recipientAccountNumberLabel.setText(pipe.getRecipientAccountNo());
        amountLabel.setText(String.format("%.2f PLN", pipe.getAmount()));
        descriptionLabel.setText(pipe.getDescription());
        if (!pipe.getApproved()) {
            approvedLabel.setText("Current state: rejected");
            circle.setFill(Color.web("F23400"));
            approveButton.setText("Approve");
        } else {
            approvedLabel.setText("Current state: approved");
            circle.setFill(Color.web("74F237"));
            approveButton.setText("Reject");
        }
    }

    /**
     * Is called by the bankPanelController to give a reference back to itself.
     *
     * @param bankPanelController
     */
    public void setParentController(BankPanelController bankPanelController) {
        this.bankPanelController = bankPanelController;
    }

    /**
     * Is called by the client application to give a reference back to itself.
     *
     * @param clientApp
     */
    public void setClientApp(ClientApp clientApp) {
        this.clientApp = clientApp;
    }

    /**
     * It is called by user to update state of approved field
     */
    @FXML
    private void handleApproved() {
        // TODO - update approved state
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Banking Application");
        alert.setHeaderText("Right now you cannot accept this pipe or any other.");
        alert.setContentText("Dear customer\nThis functionality will be implemented in the following update.\nHave a great day.");

        alert.showAndWait();
    }
}