package pl.msgs.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import pl.msgs.client.ClientApp;

import java.util.ArrayList;

public class LoginPanelController {
    @FXML
    private TextField clientIDTextField;
    @FXML
    private Button validateButton;
    @FXML
    private Button loginButton;

    // All password TextFields.
    @FXML
    private PasswordField pTF1;
    @FXML
    private PasswordField pTF2;
    @FXML
    private PasswordField pTF3;
    @FXML
    private PasswordField pTF4;
    @FXML
    private PasswordField pTF5;
    @FXML
    private PasswordField pTF6;
    @FXML
    private PasswordField pTF7;
    @FXML
    private PasswordField pTF8;
    @FXML
    private PasswordField pTF9;
    @FXML
    private PasswordField pTF10;
    @FXML
    private PasswordField pTF11;
    @FXML
    private PasswordField pTF12;
    @FXML
    private PasswordField pTF13;
    @FXML
    private PasswordField pTF14;
    @FXML
    private PasswordField pTF15;
    @FXML
    private PasswordField pTF16;
    @FXML
    private PasswordField pTF17;
    @FXML
    private PasswordField pTF18;
    @FXML
    private PasswordField pTF19;
    @FXML
    private PasswordField pTF20;
    @FXML
    private PasswordField pTF21;
    @FXML
    private PasswordField pTF22;
    @FXML
    private PasswordField pTF23;
    @FXML
    private PasswordField pTF24;
    @FXML
    private PasswordField pTF25;

    // ArrayList containing all password TextFields.
    private ArrayList<PasswordField> passwordTextFieldArray = new ArrayList<PasswordField>();

    // reference to the client application.
    private ClientApp clientApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public LoginPanelController() {
    }

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Adding all password TextFields to an ArrayList.
        passwordTextFieldArray.add(pTF1);
        passwordTextFieldArray.add(pTF2);
        passwordTextFieldArray.add(pTF3);
        passwordTextFieldArray.add(pTF4);
        passwordTextFieldArray.add(pTF5);
        passwordTextFieldArray.add(pTF6);
        passwordTextFieldArray.add(pTF7);
        passwordTextFieldArray.add(pTF8);
        passwordTextFieldArray.add(pTF9);
        passwordTextFieldArray.add(pTF10);
        passwordTextFieldArray.add(pTF11);
        passwordTextFieldArray.add(pTF12);
        passwordTextFieldArray.add(pTF13);
        passwordTextFieldArray.add(pTF14);
        passwordTextFieldArray.add(pTF15);
        passwordTextFieldArray.add(pTF16);
        passwordTextFieldArray.add(pTF17);
        passwordTextFieldArray.add(pTF18);
        passwordTextFieldArray.add(pTF19);
        passwordTextFieldArray.add(pTF20);
        passwordTextFieldArray.add(pTF21);
        passwordTextFieldArray.add(pTF22);
        passwordTextFieldArray.add(pTF23);
        passwordTextFieldArray.add(pTF24);
        passwordTextFieldArray.add(pTF25);
    }

    /**
     * Is called by the client application to give a reference back to itself.
     *
     * @param clientApp
     */
    public void setClientApp(ClientApp clientApp) {
        this.clientApp = clientApp;
    }

    /**
     * Handles first part of logging process.
     * Checks client ID if is valid.
     */
    @FXML
    private void handleValidateButton() {
        if (!clientIDTextField.getText().equals("")) {
            try {
                int clientID = Integer.parseInt(clientIDTextField.getText());

                if (clientApp.getClientConnection().checkForClient(clientID) ){
                    clientIDTextField.setDisable(true);
                    validateButton.setVisible(false);
                    validateButton.setDefaultButton(false);
                    loginButton.setVisible(true);
                    loginButton.setDefaultButton(true);
                    enableByIndex(clientApp.getClientConnection().getIndexes());
                } else {
                    clientIDTextField.setText("");
                    clientIDTextField.setPromptText("Invalid ID");
                }
            } catch (NumberFormatException e) {
                clientIDTextField.setText("");
                clientIDTextField.setPromptText("ID is a set of number");
            }
        }
    }

    /**
     * Handles second part of logging process.
     * Checks client password if is valid.
     */
    @FXML
    private void handleLoginButton() {
        if(checkPasswordByIndex(clientApp.getClientConnection().getIndexes())) {
            char[] symbols = getPasswordAsArray(clientApp.getClientConnection().getIndexes());
            try {
                if (clientApp.getClientConnection().authorizeClientConnection(symbols)) {
                    clientApp.showBankPanel();
                } else {
                    disablePasswordButtons();
                    loginButton.setVisible(false);
                    loginButton.setDefaultButton(false);
                    clientIDTextField.setDisable(false);
                    clientIDTextField.setText("");
                    validateButton.setVisible(true);
                    validateButton.setDefaultButton(true);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * Disables and clears all password TextFields.
     */
    private void disablePasswordButtons() {
        passwordTextFieldArray.forEach(textField -> {
            textField.setDisable(true);
            textField.setText("");
        });
    }

    /**
     * Enables required password TextFields - specified by indexes.
     * @param indexs
     */
    private void enableByIndex(int[] indexs) {
        KeyEvent event = new KeyEvent(KeyEvent.KEY_PRESSED, KeyCode.TAB.toString(), KeyCode.TAB.toString(), KeyCode.TAB, false, false, false, false);
        for (int i = 0; i < indexs.length; i++) {
            passwordTextFieldArray.get(indexs[i]).setDisable(false);
            final int no = i;
            passwordTextFieldArray.get(indexs[i]).lengthProperty().addListener((observable, oldValue, newValue) -> {
                if ( (newValue.intValue() == 1 && oldValue.intValue() == 0) || (newValue.intValue() == 1 && oldValue.intValue() == 1) ) {
                    passwordTextFieldArray.get(indexs[no]).fireEvent(event);
                } else if (newValue.intValue() > 1 && oldValue.intValue() == 1) {
                    passwordTextFieldArray.get(indexs[no]).setText(passwordTextFieldArray.get(indexs[no]).getText().substring(passwordTextFieldArray.get(indexs[no]).getText().length()-1));
                    passwordTextFieldArray.get(indexs[no]).fireEvent(event);
                }
            });
        }
    }

    /**
     * Checks required password TextFields - specified by indexes.
     * @param indexs
     * @return true if required TextFields are not empty.
     */
    private boolean checkPasswordByIndex(int[] indexs) {
        boolean test = true;
        for (int i = 0; i < indexs.length && test; i++) {
            if (passwordTextFieldArray.get(indexs[i]).getText().equals(""))
                test = false;
        }
        return test;
    }

    /**
     * Returns password symbols from specified indexes as a char array.
     * @param indexs
     * @return char[] array of chars at specified password indexes.
     */
    private char[] getPasswordAsArray(int[] indexs) {
        char[] symbols = new char[indexs.length];
        for (int i = 0; i < indexs.length; i++) {
            symbols[i] = passwordTextFieldArray.get(indexs[i]).getText().charAt(0);
        }
        return symbols;
    }
}
