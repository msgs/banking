package pl.msgs.client;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pl.msgs.client.view.BankPanelController;
import pl.msgs.client.view.LoginPanelController;
import pl.msgs.model.Person;


/**
 * Main client's application for working with his/her bank account.
 */
public class ClientApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private ClientConnection clientConnection;
    private int port = 4646;
    private Person client;
    private Logger logger = Logger.getLogger(ClientApp.class.getName());

    private BankPanelController bankPanelController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Banking Application");

        initRootLayout();

        showLoginPanel();
    }

    /**
     * Initializes the root layout.
     */
    private void initRootLayout() {
        try {
            // Load the root layout form the fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClientApp.class.getResource("view/RootClientLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setOnCloseRequest(event -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.initOwner(primaryStage);
                alert.setTitle("Banking application closing");
                alert.setHeaderText("Are you sure you want to close banking application");
                alert.setContentText("");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.CANCEL) {
                    event.consume();
                }
                exitApp();
            });
            clientConnection = new ClientConnection(port, this);
        } catch (IOException e) {
            logger.warning(e.toString());
            showConnectionError();
        }
    }

    /**
     * Shows login panel inside the root layout.
     */
    public void showLoginPanel() {
        try {
            // Load login panel
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClientApp.class.getResource("view/LoginPanel.fxml"));
            AnchorPane loginPanel = (AnchorPane) loader.load();

            // Show login panel inside the root layout.
            rootLayout.setCenter(loginPanel);

            // Give the controller access to the main app.
            LoginPanelController controller = loader.getController();
            controller.setClientApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows bank panel inside the root layout.
     */
    public void showBankPanel() {
        try {
            // Load login panel
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClientApp.class.getResource("view/BankPanel.fxml"));
            AnchorPane bankPanel = (AnchorPane) loader.load();

            // Show login panel inside the root layout.
            rootLayout.setCenter(bankPanel);

            // Give the controller access to the main app.
            bankPanelController = loader.getController();
            bankPanelController.setClientApp(this);
            bankPanelController.showClientData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showAccount() {
        bankPanelController.handleAccounts();
    }

    /**
     * Returns the client connection.
     *
     * @return clientConnection.
     */
    public ClientConnection getClientConnection() {
        return clientConnection;
    }

    /**
     * Returns the main stage.
     *
     * @return primaryStage of client's view
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Generates alert when application could not connect to the server.
     */
    public void showConnectionError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Banking application error");
        alert.setHeaderText("Couldn't connect to the server.");
        alert.setContentText("Try again later.\n\nIf this error reappears again please contact with our support team.");
        alert.setOnCloseRequest(event -> {
            exitApp();
            alert.close();
            primaryStage.close();
        });

        alert.showAndWait();
    }

    /**
     * Generates alert when application could not connect to the server and download client database.
     */
    public void showRefreshError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Banking application error");
        alert.setHeaderText("Could not refresh data.");
        alert.setContentText("Try again later.\n\nIf this error reappears again please contact with our support team.");
        alert.setOnCloseRequest(event -> {
            exitApp();
            alert.close();
            primaryStage.close();
        });
        alert.showAndWait();
    }

    /**
     * Close connection with a server and then app can be close.
     */
    private void exitApp() {
        clientConnection.exitApp();
    }

    // Getter and setter of a client.
    public Person getClient() {
        return client;
    }

    public void setClient(Person client) {
        this.client = client;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
