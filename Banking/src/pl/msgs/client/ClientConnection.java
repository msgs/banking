package pl.msgs.client;

import pl.msgs.model.Account;
import pl.msgs.model.HistoryEvent;
import pl.msgs.model.Person;
import pl.msgs.util.StringToArray;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Scanner;

/**
 * The class that allows us to connect to the server via socket connection.
 */
public class ClientConnection {
    private final int port;
    private static String host = "77.55.208.177";     // "77.55.208.177" || "localhost"
    private Socket socket;
    private InputStream inStream;
    private OutputStream outStream;
    private Scanner in;
    private PrintWriter out;
    private ClientApp clientApp;
    private boolean connectionState;

    public ClientConnection(int port, ClientApp clientApp) throws IOException {
        this.port = port;
        this.clientApp = clientApp;
        socket = new Socket(host, this.port);
        inStream = socket.getInputStream();
        outStream = socket.getOutputStream();
        in = new Scanner(inStream, "UTF-16");
        out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);
        connectionState = false;

    }

    // After .checkForClient() method contains indexes of password for authorization.
    private int[] indexes;

    /**
     * Checks if the client is in the database.
     * Shows indexes of password symbols needed to be passed to server for authorization - int[] indexes.
     *
     * @param clientID
     * @return true when client exist in the database.
     */
    public boolean checkForClient(int clientID) {
        // If server is ready.
        out.println("1");

        int input = Integer.parseInt(in.nextLine());
        if (input == 1) {
            //Server ready. Ask for client.
            out.println(clientID);
            input = Integer.parseInt(in.nextLine());
            if (input == 1) {
                // Fill indexes array.
                out.println("1");
                indexes = StringToArray.stringToIntArray(in.nextLine(), ",");
                return true;
            }
        }
        return false;
    }

    /**
     * Authorizes client password with server by sending required symbols of password.
     *
     * @param symbols required symbols of password.
     * @return true when password is authorized.
     */
    public boolean authorizeClientConnection(char[] symbols) throws IOException, ClassNotFoundException {
        // Send server an array of char contains password symbols at indexes.
        out.println(Arrays.toString(symbols));
        int input = Integer.parseInt(in.nextLine());
        if (input == 1) {
            out.println("1");
            String temp = in.nextLine();
            byte[] data = Base64.getDecoder().decode(temp);
            ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(data));
            clientApp.setClient((Person) oin.readObject());
            oin.close();
            connectionState = true;
            return true;
        }
        return false;
    }

    /**
     * Refreshes client data.
     *
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void refreshData() throws IOException, ClassNotFoundException {
        out.println("1");
        if (Integer.parseInt(in.nextLine()) == 1) {
            out.println("1");
            String temp = in.nextLine();
            if (!temp.equals("0")) {
                byte[] data = Base64.getDecoder().decode(temp);
                ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(data));
                clientApp.setClient((Person) oin.readObject());
                oin.close();
            }
        }
    }

    /**
     * Makes transfer via server connection.
     *
     * @param account_destinationAccount_value_title_comment_array separated by ";;;;".
     * @return 1 if success; 0 if sending account was wrong; 2 - transfer value 0; 3 - not enough money; 4 - No connection.
     */
    public int makeTransfer(String account_destinationAccount_value_title_comment_array) {
        out.println("2");
        if (Integer.parseInt(in.nextLine()) == 1) {
            out.println(account_destinationAccount_value_title_comment_array);
            int answer = Integer.parseInt(in.nextLine());
            return answer;
        }
        return 4;
    }

    /**
     * Logout the client.
     *
     * @return true when logout was successful.
     */
    public boolean logout() {
        connectionState = false;
        out.println("4");
        if (Integer.parseInt(in.nextLine()) == 1) {
            out.println("1");
            if (Integer.parseInt(in.nextLine()) == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Logout the client and sends a message to the server that the connection wil be close soon.
     *
     * @return true when logout was successful.
     */
    public boolean exitApp() {
        if (connectionState) {
            out.println("25");
            if (Integer.parseInt(in.nextLine()) == 1) {
                out.println("1");
                if (Integer.parseInt(in.nextLine()) == 1) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    // Getter
    public int[] getIndexes() {
        return indexes;
    }

    public ArrayList<HistoryEvent> getAccountHistory(Account account) throws IOException, ClassNotFoundException {
        ArrayList<HistoryEvent> history = null;
        out.println("3");
        if (Integer.parseInt(in.nextLine()) == 1) {
            out.println(account.getAccountNumber());
            String temp = in.nextLine();
            if (!temp.equals("0")) {
                byte[] data = Base64.getDecoder().decode(temp);
                ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(data));
                history = (ArrayList<HistoryEvent>) oin.readObject();
                oin.close();
            }
        }
        return history;
    }
}
