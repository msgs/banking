package pl.msgs.util;

/**
 * Class for conversion of String containing array to array of specified type.
 */
public class StringToArray {

    /**
     * Helper function to convert int array as String to int array.
     * @param arrayAsString String consisting int array, ie "[1, 5, 1024]".
     * @param separator String consisting separator symbols, ie ",".
     * @return int[] array of int.
     */
    public static int[] stringToIntArray(String arrayAsString, String separator) {
        String[] input = arrayAsString.replaceFirst("\\[", "").replaceFirst("\\]", "").replaceAll("\\s", "").split(separator);
        int[] output = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = Integer.parseInt(input[i]);
        }
        return output;
    }

    /**
     * Helper function to convert char array as String to char array.
     * @param arrayAsString String consisting char array, ie "[a, d, 7]".
     * @param separator String consisting separator symbols, ie ",".
     * @return char[] array of char.
     */
    public static char[] stringToCharArray(String arrayAsString, String separator) {
        String[] input = arrayAsString.replaceFirst("\\[", "").replaceFirst("\\]", "").replaceAll("\\s", "").split(separator);
        char[] output = new char[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = input[i].charAt(0);
        }
        return output;
    }

    /**
     * Helper function to convert String array as String to String array.
     * @param arrayAsString String consisting String array, ie "[test, d, 76dtk]".
     * @param separator String consisting separator symbols, ie ",".
     * @return String[] array of String.
     */
    public static String[] stringToStringArray(String arrayAsString, String separator) {
        String[] output = arrayAsString.replaceFirst("\\[", "").replaceFirst("\\]", "").split(separator);
        return output;
    }
}
