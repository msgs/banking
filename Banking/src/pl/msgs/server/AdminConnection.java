package pl.msgs.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * The class that creates server socket connections waiting for administrator host connections on port defined by the port variable.
 */
public class AdminConnection implements Runnable {
    private volatile boolean running = true;
    private Thread thread;
    private final int port = 4647;
    private volatile int adminCount = 0;
    private Logger logger = Logger.getLogger(AdminConnection.class.getName());

    /**
     * Starts the server connection.
     */
    public void startConnection() {
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Close the admin-server connection.
     * Decreases the admin count number.
     */
    public void closeAdminConnection() {
        logger.info("Thread has deleted a administrator handling thread no: " + adminCount);
        adminCount--;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (running) {
                Socket incoming = serverSocket.accept();
                logger.info("Thread has spawning new administrator handling thread no " + ++adminCount);
                AdminHandler adminHandler = new AdminHandler(incoming, this);
                adminHandler.startConnection();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("End of client connection thread.");

    }

    public int getAdminCount() {
        return adminCount;
    }
}