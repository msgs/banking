package pl.msgs.server;

import pl.msgs.model.Account;
import pl.msgs.model.HistoryEvent;
import pl.msgs.model.Person;
import pl.msgs.util.StringToArray;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

public class ClientTalkProtocol {
    private static final String SUCCESS = "1";
    private static final String FAILURE = "0";

    // Server is waiting for client connection.
    private static final int WAITING = 0;
    // Server is waiting for client login number.
    private static final int WAITING_FOR_LOGIN = 1;
    // Server is sending indexes of password to authenticate.
    private static final int WAITING_TO_SEND_INDEXES = 2;
    // Server is waiting for client password.
    private static final int WAITING_FOR_PASSWORD = 3;
    // Server is ready to send client data.
    private static final int SENDING_DATA = 4;
    // Server is connected with client. Connection is authorized.
    private static final int CONNECTED = 5;

    // Variable connected with CONNECTED.
    private int connectedIndex;

    private int state = WAITING;
    private Person client;
    private int[] indexes;      // Used for client  authorization - password.

    private ClientHandler clientHandler;

    public ClientTalkProtocol(ClientHandler clientHandler) {
        this.clientHandler = clientHandler;
    }

    public String processInput(String input) {
        String output = FAILURE;

        // Beginning of the connection.
        if (state == WAITING) {
            if (Integer.parseInt(input) == 1) {
                output = SUCCESS;
                state = WAITING_FOR_LOGIN;
            }

            // Checking for client in database.
        } else if (state == WAITING_FOR_LOGIN) {
            int clientID = Integer.parseInt(input);
            client = Server.getClientDatabase().getClientByID(clientID);
            if (client != null) {
                output = SUCCESS;
                state = WAITING_TO_SEND_INDEXES;
            } else {
                output = FAILURE;
                state = WAITING;
            }

            // Sending indexes of password to client to authenticate.
        } else if (state == WAITING_TO_SEND_INDEXES) {
            if (Integer.parseInt(input) == 1) {
                // Send indexes as a string to a client.
                indexes = client.getLoginData().getIndexesToCheck();
                output = Arrays.toString(indexes);
                state = WAITING_FOR_PASSWORD;
            }


            // Authenticate client's connection via password.
        } else if (state == WAITING_FOR_PASSWORD) {
            char[] symbols = StringToArray.stringToCharArray(input, ",");
            if (client.getLoginData().checkPersonPasswordByIndexes(indexes, symbols)) {
                output = SUCCESS;
                state = SENDING_DATA;
            } else {
                output = FAILURE;
                state = WAITING;
            }

            // Sending client data
        } else if (state == SENDING_DATA) {
            if (Integer.parseInt(input) == 1) {
                try {
                    ByteArrayOutputStream baout = new ByteArrayOutputStream();
                    ObjectOutputStream oout = new ObjectOutputStream(baout);
                    oout.writeObject(client);
                    output = Base64.getEncoder().encodeToString(baout.toByteArray());
                    oout.close();
                    state = CONNECTED;
                    connectedIndex = 0;
                } catch (IOException e) {
                    output = "";
                    state = WAITING;
                }
            }

        } else if (state == CONNECTED) {
            if (connectedIndex == 0) {
                connectedIndex = Integer.parseInt(input);
                return SUCCESS;
            }

            // Process input. 1 - refresh client data, 2 - transfer, 3 - request account history,
            // 4 - logout, 25 - close connection.
            if (connectedIndex == 1) {
                if (Integer.parseInt(input) == 1) {
                    try {
                        ByteArrayOutputStream baout = new ByteArrayOutputStream();
                        ObjectOutputStream oout = new ObjectOutputStream(baout);
                        oout.writeObject(client);
                        output = Base64.getEncoder().encodeToString(baout.toByteArray());
                        oout.close();
                    } catch (IOException e) {
                        output = FAILURE;
                    }
                }
                state = CONNECTED;
                connectedIndex = 0;

            } else if (connectedIndex == 2) {
                String[] transferData = StringToArray.stringToStringArray(input, ";;;;");
                if (Account.getAccountByNumber(transferData[0]) == null) {
                    state = CONNECTED;
                    connectedIndex = 0;
                    return FAILURE;
                }

                Account accountSending = Account.getAccountByNumber(transferData[0]);
                Double value;
                try {
                    value = Double.parseDouble(transferData[2]);
                } catch (NumberFormatException e) {
                    state = CONNECTED;
                    connectedIndex = 0;
                    return FAILURE;
                }

                int answer = Server.makeTransfer(accountSending, transferData[1], value, transferData[3], transferData[4]);
                output = Integer.toString(answer);
                state = CONNECTED;
                connectedIndex = 0;

            } else if (connectedIndex == 3) {
                Account temp = Account.getAccountByNumber(input);
                if (temp != null) {
                    ArrayList<HistoryEvent> history = Server.getAccountHistory(temp);

                    try {
                        ByteArrayOutputStream baout = new ByteArrayOutputStream();
                        ObjectOutputStream oout = new ObjectOutputStream(baout);
                        oout.writeObject(history);
                        output = Base64.getEncoder().encodeToString(baout.toByteArray());
                        oout.close();
                    } catch (IOException e) {
                        output = FAILURE;
                    }
                } else {
                    output = FAILURE;
                }
                state = CONNECTED;
                connectedIndex = 0;

            } else if (connectedIndex == 4) {
                client = null;
                output = SUCCESS;
                state = WAITING;
                connectedIndex = 0;


            } else if (connectedIndex == 25) {
                client = null;
                clientHandler.closeConnection();
                output = SUCCESS;
                state = WAITING;
                connectedIndex = 0;
            }
        }


        return output;
    }


}
