package pl.msgs.server;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * The class that creates local server socket connections waiting for server user connections on port defined by the port variable.
 */
public class UserConnection implements Runnable {
    private volatile boolean running = true;
    private volatile boolean reading = true;
    private Thread thread;
    private final int port = 4648;
    private Server server;
    private Logger logger = Logger.getLogger(UserConnection.class.getName());

    /**
     * Starts the server connection.
     */
    public void startConnection() {
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try (
                // Only local connections. One connection at the same time.
                ServerSocket serverSocket = new ServerSocket(port, 0, InetAddress.getByName(null));
        ) {
            while (running) {
                Socket user = serverSocket.accept();
                InputStream inStream = user.getInputStream();
                OutputStream outStream = user.getOutputStream();

                logger.warning("User is locally connected to the server");

                // Main working section - processing user commands.
                Scanner userInput = new Scanner(inStream, "UTF-16");
                PrintWriter out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);

                out.println("Connection with server is established.");
                out.println("If you need information write \"info\".");

                String input, output;
                UserTalkProtocol userTalkProtocol = new UserTalkProtocol();
                userTalkProtocol.setUserConnection(this);


                reading = true;
                while (reading) {
                    input = userInput.nextLine();
                    output = userTalkProtocol.processInput(input);
                    out.println(output);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.warning("Thread is off.");
    }

    /**
     * After calling this function this thread will be stopped soon.
     */
    public void stop() {
        reading = false;
        running = false;
        Server.stopServer();
    }

    /**
     * After calling this function the server will stop the current connection soon.
     */
    public void stopReading() {
        reading = false;
    }
}