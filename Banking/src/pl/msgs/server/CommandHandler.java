package pl.msgs.server;


import pl.msgs.model.*;
import pl.msgs.model.exception.AccountException;
import pl.msgs.server.exception.CommandException;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Class commandHandler man more complicated commands in connection with serverApp.
 *
 */
public class CommandHandler {
    private final String GOODBYE = "Goodbye";
    private static final String EXIT = "-exit";
    private static final String STOP = "-stop";
    private static final String SHOW = "-show";
    private static final String ADD = "-add";
    private static final String DELETE = "-delete";
    private static final String CONNECTION = "-connection";
    private ClientConnection clientConnection;
    private AdministratorDigitalKey adminKey;
    private AccountFactory accountFactory;
    private Administrator admin;
    private Account account;
    private Person client;
    private Server server;
    private String output;
    private boolean test;

    public CommandHandler() {
    }



    public String information(String[] arrayInput) throws CommandException {
        if (arrayInput.length > 1) {
            switch (arrayInput[1]) {
                case EXIT:
                    output = "This command is turning off the server application.";
                    break;
                case STOP:
                    output = "This command is shutting server down and turning off the server application.";
                    break;
                case SHOW:
                    output = "Shows information form server. If you need more information, you should use \"show --help\" command.";
                    break;
                case ADD:
                    output = "This command is used to add clients, admins or accounts. If you need more information, you should use \"add --help\" command.";
                    break;
                case DELETE:
                    output = "This command is used to delete clients, admins or accounts. If you need more information, you should use \"delete --help\" command.";
                    break;
                case CONNECTION:
                    output = "This command is used to connect banking server with outside services. If you need more information, you should use \"connection --help\" command.";
                    break;
                default:
                    throw new CommandException(101); //incorrect command, lack of this option

            }
        } else if (arrayInput.length > 2) {
            throw new CommandException(102); //incorrect command, too much options
        } else {
            output = "This is application which control banking server. If you need information about available commands. Use one of options \"-show\", \"-exit\", \"-stop\", \"-add\", \"-delete\", \"-connection\".";
        }
        return output;
    }

    public String showInformation(String[] arrayInput) throws CommandException {
        if (arrayInput.length > 2) {
            switch (arrayInput[1]) {
                case "-c":         //looking for user in server database
                    try {
                        int i = Integer.parseInt(arrayInput[2]);
                        test = true;
                    } catch (NumberFormatException nfe) {
                        test = false;
                    }
                    if (test) {
                        client = server.getClientDatabase().getClientByID(Integer.parseInt(arrayInput[2]));
                        if (client == null) {
                            output = "The client does not exist.";
                        } else {
                            output = arrayInput[2] + " is in database. Client: " + client.getFirstName() + " " + client.getLastName()
                                    + ", Personal ID Number: " + client.getPersonIDNumber() + ", Phone: " + client.getPhoneNumber();
                        }
                        break;
                    } else {
                        throw new CommandException(106); //Incorrect command
                    }
                case "-cp":         //looking for user in server database
                    client = server.getClientDatabase().getClientByPersonID(arrayInput[2]);
                    if (client == null) {
                        output = "The client does not exist.";
                    } else {
                        output = arrayInput[2] + " is in database. Client: " + client.getFirstName() + " " + client.getLastName()
                                + ", client ID: " + client.getLoginData().getPersonID() + ", Phone: " + client.getPhoneNumber() + ", Address: "
                                + client.getHomeAddress().getBuilding() + "/" + client.getHomeAddress().getFlat() + " " + client.getHomeAddress().getStreet() + " street "
                                + client.getHomeAddress().getZipCode() + " " + client.getHomeAddress().getCity() + ", " + client.getHomeAddress().getCountry();
                    }
                    break;
                case "-a":
                    if (arrayInput.length == 3){
                        account = account.getAccountByNumber(arrayInput[2]);
                        output = "Account nr: " + account.getAccountNumber() + "Owner is: " + account.getOwner().getLoginData().getPersonID() + " " + account.getOwner().getFirstName() + " "
                                + account.getOwner().getLastName() + " Balance: " + account.getBalance();
                    }else{
                        throw new CommandException(106); //Incorrect command
                    }
                    break;
                case "-admin":      //looking for admin in server database
                    if (arrayInput.length == 3){
                        admin = server.getAdminDatabase().getAdminByID(Integer.parseInt(arrayInput[2]));
                        output = arrayInput[2] + " is in database. Admin: " + admin.getFirstName() + " " + admin.getLastName() + " Phone: " + admin.getPhoneNumber();
                    }else
                        throw new CommandException(105); //incorrect command
                    break;
                default:
                    throw new CommandException(105); //Incorrect command

            }
        } else if (arrayInput.length == 2) {
            switch (arrayInput[1]) {
                case "-t":
                    output = "Server started -- " + server.getTime();
                    break;
                case "-clients":
                    output = "Number of logged clients: " + clientConnection.getClientCount();
                    break;
                case "--help":
                    output = "Command \"show\" shows information form server. Available options is \"-c Client_ID\" :: looking for clients by client id;" +
                            " \"-cp Personal_ID_Number\" :: looking for clients by personal id number; \"-a Account_Number\" :: looking for accounts; " +
                            " \"-t\" :: time when server was started; \"-clients\" :: shows number of logged clients; \"-admin Admin_ID\" :: looking for administrators by admin id";
                    break;
                default:
                    throw new CommandException(105); //incorrect command
            }

        }else{
            throw new CommandException(104); //Incomplete command
        }

        return output;
    }

    public String add(String[] arrayInput) throws CommandException{
        if (arrayInput.length > 2) switch (arrayInput[1]) {
            case "-admin":
                if (arrayInput.length == 6) {
                    admin = new Administrator(arrayInput[2], arrayInput[3], arrayInput[4], arrayInput[5]);
                    server.getAdminDatabase().add(admin);
                    try {
                        adminKey.setPathToFolder(server.getKeysPath());
                        adminKey = new AdministratorDigitalKey(admin, LocalDate.now(), LocalTime.now(), server.getTime());
                    } catch (IOException ex) {
                        throw new CommandException(111); //admin was not delete
                    }
                    output = "Added Administrator " + arrayInput[2] + " " + arrayInput[3] + " admin ID: " + admin.getId();
                } else {
                    throw new CommandException(108); //incorrect command
                }
                break;
            case "-client":
                if (arrayInput.length == 16) {
                    client = new Person(arrayInput[2], arrayInput[3], Integer.parseInt(arrayInput[4]), Integer.parseInt(arrayInput[5]), Integer.parseInt(arrayInput[6]),
                            arrayInput[7], arrayInput[8], arrayInput[9], arrayInput[10], arrayInput[11], arrayInput[12], arrayInput[13], arrayInput[14], arrayInput[15]);
                    server.getClientDatabase().add(client);
                    output = "Added Client " + arrayInput[2] + " " + arrayInput[3] + " Client ID: " + client.getLoginData().getPersonID();
                } else {
                    throw new CommandException(108); //incorrect command
                }
                break;
            case "-account":
                if (arrayInput.length == 6) {
                    try {
                        int i = Integer.parseInt(arrayInput[2]);
                        test = true;
                    } catch (NumberFormatException nfe) {
                        test = false;
                    }
                    if (test) {
                        client = server.getClientDatabase().getClientByID(Integer.parseInt(arrayInput[2]));
                        if (client == null) {
                            output = "The client does not exist.";
                        } else {
                            try {
                                Account account = accountFactory.getAccountWithBalance(arrayInput[3], client, Double.parseDouble(arrayInput[4]), Double.parseDouble(arrayInput[5]));
                                output = "Added account nr:" + client.addAccount(account) + " to client (ID): " + client.getLoginData().getPersonID();
                            }catch (AccountException aex){
                                output = aex.getMessage() + "Probably type of account is unknown.";
                            }
                        }
                        break;
                    } else {
                        throw new CommandException(108); //Incorrect command
                    }
                } else {
                    throw new CommandException(108); //incorrect command
                }
            default:
                throw new CommandException(107); //Incomplete command
        }
        else if (arrayInput.length == 2){
            switch (arrayInput[1]){
                case "--help":
                    output = "If you want add: ACCOUNT use option -account [client Id] [type (standard, placing credit)] [balance] [bankRate] ; CLIENTS use option -client [FirstName] [LastName] [birthdayDay] [birthdayMonth] [birthdayYear]"
                            +" [street] [building] [flat] [zipCode] [city] [country] [phoneNumber] [personIDnumber] [Password]; ADMINISTRATOR use option -admin [FirstName] [LastName] [phoneNumber] [password].";
                    break;
                default:
                    throw new CommandException(108); //Incorrect command
            }
        }else{
            throw new CommandException(107); //Incomplete command
        }
        return output;
    }

    public String delete(String[] arrayInput) throws CommandException{
        if (arrayInput.length > 2){
            switch (arrayInput[1]){
                case "-admin":
                    if (arrayInput.length == 3) {
                        admin = server.getAdminDatabase().getAdminByID(Integer.parseInt(arrayInput[2]));
                        server.getAdminDatabase().remove(server.getAdminDatabase().indexOf(admin));
                        output = "Deleted Administrator ID: " + arrayInput[2];
                    }else{
                        throw new CommandException(110); //incorrect command
                    }
                    break;
                case "-client":
                    if (arrayInput.length == 3){
                        client = server.getClientDatabase().getClientByID(Integer.parseInt(arrayInput[2]));
                        server.getClientDatabase().remove(server.getClientDatabase().indexOf(client));
                        output = "Deleted Client ID: " + arrayInput[2];
                    }else {
                        throw  new CommandException(110); //incorrect command
                    }
                    break;
                case "-account":
                    if (arrayInput.length == 4 ){
                        client = server.getClientDatabase().getClientByID(Integer.parseInt(arrayInput[2]));
                        account = account.getAccountByNumber(arrayInput[3]);
                        client.getAccounts().remove(client.getAccounts().indexOf(account));
                        output = "Account nr: " + arrayInput[3] + "was deleted.";
                    }else{
                        throw  new CommandException(110); //incorrect command
                    }
                default:
                    throw new CommandException(109); //Incomplete command
            }
        }else if (arrayInput.length == 2){
            switch (arrayInput[1]){
                case "--help":
                    output = "If you want delete: ACCOUNT use option -account [clientId] [accountNumber] ; CLIENTS use option -client [clientID]; ADMINISTRATOR use option -admin [adminID].";
                    break;
                default:
                    throw new CommandException(110); //Incorrect command
            }
        }else{
            throw new CommandException(109); //Incomplete command
        }
        return output;
    }

    public String connection(String[] arrayInput) throws CommandException {
        int time;
        if (arrayInput.length > 2) {
            switch (arrayInput[1]) {
                case "-start":
                        switch (arrayInput[2]){
                            case "FT":
                                time = Integer.parseInt(arrayInput[3]);
                                time = time * 1000;
                                Server.startFtConnection(time);
                                output = "Connected to FT - refresh time=" + time/1000 + " sec.";
                                break;
                            default:
                                throw new CommandException(115);
                        }
                    break;
                case "-stop":
                    switch (arrayInput[2]){
                        case "FT":
                            Server.stopFtConnection();
                            output = "Disconnected with FT.";
                            break;
                        default:
                            throw new CommandException(115);
                    }
                    break;
                default:
                    throw new CommandException(114); //Incomplete command

            }
        }else if (arrayInput.length == 2){
            switch (arrayInput[1]){
                case "--help":
                    output = "If you want connect or disconnect to outside services: CONNECT use option -start [service_name] [refresh_time_in_seconds]; DISCONNECT use option -stop [service_name]. If you want to see available systems use option --available.";
                    break;
                case "--available":
                    output = "Available system is - system_name -> FT";
                    break;
                default:
                    throw new CommandException(115); //Incorrect command
            }

        }else {
            throw new CommandException(114); //Incomplete command
        }
        return output;
    }

}
