package pl.msgs.server;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * The class that handles communication with individual client application.
 */
public class ClientHandler implements Runnable {
    private volatile boolean running = true;
    private Thread thread;
    private Socket socket;
    private ClientConnection clientConnection;

    /**
     * Constructor of this class.
     *
     * @param socket client socket.
     */
    public ClientHandler(Socket socket, ClientConnection clientConnection) {
        this.socket = socket;
        this.clientConnection = clientConnection;
    }

    /**
     * Starts the client-server connection.
     */
    public void startConnection() {
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Close the client-server connection.
     */
    public void closeConnection() {
        running = false;
    }

    @Override
    public void run() {
        try (
                InputStream inStream = socket.getInputStream();
                OutputStream outStream = socket.getOutputStream();
        ) {
            Scanner in = new Scanner(inStream, "UTF-16");
            PrintWriter out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);

            String input, output;
            ClientTalkProtocol talkProtocol = new ClientTalkProtocol(this);

            while (running) {
                input = in.nextLine();
                output = talkProtocol.processInput(input);
                out.println(output);
            }

            in.close();
            out.close();
            clientConnection.closeClientConnection();

        } catch (NoSuchElementException e) {
            clientConnection.closeClientConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
