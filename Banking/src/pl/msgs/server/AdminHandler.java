package pl.msgs.server;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * The class that handles communication with individual administrator application.
 */
public class AdminHandler implements Runnable {
    private volatile boolean running = true;
    private Thread thread;
    private Socket socket;
    private AdminConnection adminConnection;

    /**
     * Constructor of this class.
     *
     * @param socket administrator socket.
     */
    public AdminHandler(Socket socket, AdminConnection adminConnection) {
        this.socket = socket;
        this.adminConnection = adminConnection;
    }

    /**
     * Starts the administrator-server connection.
     */
    public void startConnection() {
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Close the admin-server connection.
     */
    public void closeConnection() {
        running = false;
    }

    @Override
    public void run() {
        try (
                InputStream inStream = socket.getInputStream();
                OutputStream outStream = socket.getOutputStream();
        ) {
            Scanner in = new Scanner(inStream, "UTF-16");
            PrintWriter out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);

            String input, output;
            AdminTalkProtocol talkProtocol = new AdminTalkProtocol(this);

            while (running) {
                input = in.nextLine();
                output = talkProtocol.processInput(input);
                out.println(output);
            }

            in.close();
            out.close();
            adminConnection.closeAdminConnection();

        } catch (NoSuchElementException e) {
            adminConnection.closeAdminConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
