package pl.msgs.server;

import pl.msgs.server.exception.CommandException;
import pl.msgs.util.StringToArray;
import java.lang.String;
import java.util.logging.Logger;

public class UserTalkProtocol {

    private CommandHandler commandHandler = new CommandHandler();
    private UserConnection userConnection;
    private static final String EXIT = "exit";
    private static final String STOP = "stop";
    private static final String SHOW = "show";
    private static final String INFORMATION = "info";
    private static final String ADD = "add";
    private static final String DELETE = "delete";
    private static final String CONNECTION =  "connection";
    private Logger logger = Logger.getLogger(UserTalkProtocol.class.getName());

    public String processInput(String input) {
        String[] arrayInput = StringToArray.stringToStringArray(input, "\\s+");
        String output = "";


        switch(arrayInput[0]){
            // End of server's work.
            case EXIT:
                userConnection.stopReading();
                output = "Goodbye";
                break;
            // End of server's work.
            case STOP:
                userConnection.stop();
                output = "Goodbye";
                break;
            //information about available commands
            case INFORMATION:
                try{
                    output = commandHandler.information(arrayInput);
                }catch (CommandException cex){
                    output = cex.getMessage();
                }
                break;
            // showing present information about server
            case SHOW:
                try {
                    output = commandHandler.showInformation(arrayInput);
                }catch (CommandException cex){
                    output = cex.getMessage();
                }
                break;
            // adding clients, admins, accounts etc.
            case ADD:
                try{
                    output = commandHandler.add(arrayInput);
                }catch (CommandException cex){
                    output = cex.getMessage();
                }
                break;
            // deleting clients, admins, accounts etc.
            case DELETE:
                try{
                    output = commandHandler.delete(arrayInput);
                }catch (CommandException cex){
                    output = cex.getMessage();
                }
                break;
            // starting connections with outside services
            case CONNECTION:
                try {
                    output = commandHandler.connection(arrayInput);
                }catch (CommandException cex){
                    output = cex.getMessage();
                }
                break;
                default:
                    output = "Error: Unknown command \"" + input + "\"";
                    logger.warning("Unrecognized command from serverApp: \"" + input + "\"");

        }


        return output;
    }

    /**
     * Is called by the UserConnection to give a reference back to itself.
     * @param userConnection
     */
    public void setUserConnection(UserConnection userConnection) {
        this.userConnection = userConnection;
    }
}
