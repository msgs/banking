package pl.msgs.server;

import pl.msgs.model.Administrator;
import pl.msgs.model.AdministratorDigitalKey;
import pl.msgs.model.Person;
import pl.msgs.util.StringToArray;

import java.io.IOException;
import java.util.Arrays;

public class AdminTalkProtocol {
    private static final String SUCCESS = "1";
    private static final String FAILURE = "0";

    // Server is waiting for administrator connection.
    private static final int WAITING = 0;
    // Server is waiting for administrator login number.
    private static final int WAITING_FOR_LOGIN = 1;
    // Server is sending indexes of password to authenticate.
    private static final int WAITING_TO_SEND_INDEXES = 2;
    // Server is waiting for administrator password.
    private static final int WAITING_FOR_PASSWORD = 3;
    // Server is waiting for administrator key.
    private static final int WAITING_FOR_KEY = 4;
    // Server is connected with administrator. Connection is authorized.
    private static final int CONNECTED = 6;

    // Variable connected with CONNECTED.
    private int connectedIndex;
    // Server is ready to send client data to administrator.
    private static final int SENDING_DATA = 1;
    // Variable connected with SENDING_DATA.
    private int dataIndex;

    // String containing end of transfer message;
    final String endSignal = "*END!OF!DATABASE*";


    private int state = WAITING;
    private Administrator admin;
    private int[] indexes;      // Used for administrator  authorization - password.

    private AdminHandler adminHandler;

    public AdminTalkProtocol(AdminHandler adminHandler) {
        this.adminHandler = adminHandler;
    }

    public String processInput(String input) {
        String output = FAILURE;

        // Beginning of the connection.
        if (state == WAITING) {
            if (Integer.parseInt(input) == 1) {
                output = SUCCESS;
                state = WAITING_FOR_LOGIN;
            }

            // Checking for administrator in database.
        } else if (state == WAITING_FOR_LOGIN) {
            int adminID = Integer.parseInt(input);
            admin = Server.getAdminDatabase().getAdminByID(adminID);
            if (admin != null) {
                output = SUCCESS;
                state = WAITING_TO_SEND_INDEXES;
            } else {
                output = FAILURE;
                state = WAITING;
            }

            // Sending indexes of password to administrator to authenticate.
        } else if (state == WAITING_TO_SEND_INDEXES) {
            if (Integer.parseInt(input) == 1) {
                // Send indexes as a string to a administrator.
                indexes = admin.getIndexesToCheck();
                output = Arrays.toString(indexes);
                state = WAITING_FOR_PASSWORD;
            }


            // Authenticate administrator's password.
        } else if (state == WAITING_FOR_PASSWORD) {
            char[] symbols = StringToArray.stringToCharArray(input, ",");
            if (admin.checkAdminPasswordByIndexes(indexes, symbols)) {
                output = SUCCESS;
                state = WAITING_FOR_KEY;
            } else {
                output = FAILURE;
                state = WAITING;
            }

            // Authenticate administrator's connection via digital key.
        } else if (state == WAITING_FOR_KEY) {
            try {
                AdministratorDigitalKey keyFromAdmin;
                keyFromAdmin = AdministratorDigitalKey.getAdminKeyFromString(input);

                if (AdministratorDigitalKey.getAdminKey(admin).validateKey(keyFromAdmin)) {
                    output = SUCCESS;
                    state = CONNECTED;
                    connectedIndex = 0;
                } else {
                    output = FAILURE;
                    state = WAITING;
                }
            } catch (Exception e) {
                output = FAILURE;
                state = WAITING;
            }

            // Sending required data to administrator.
            // connectedIndex: 1 - sending client database, 2 - updating local client database, 3 - logout, 25 - close connection.
        } else if (state == CONNECTED) {
            if (connectedIndex == 0) {
                connectedIndex = Integer.parseInt(input);
                return Integer.toString(1);
            }

            // Sending client data to administrator
            if (connectedIndex == SENDING_DATA) {
                // 1 = request data, beginning of connection; 2 = request next; 3 = end of transmission.
                if (Integer.parseInt(input) == 1) {
                    dataIndex = 0;
                    output = endSignal;

                } else if (Integer.parseInt(input) == 2) {
                    if (dataIndex < Server.getClientDatabase().size()) {
                        try {
                            output = Server.getClientDatabase().get(dataIndex).personToCodedString();
                            dataIndex++;
                        } catch (IOException e) {
                            output = "ERROR";
                            connectedIndex = 0;
                            dataIndex = 0;
                        }
                    } else {
                        output = endSignal;
                        connectedIndex = 0;
                        dataIndex = 0;
                    }
                } else if (Integer.parseInt(input) == 3) {
                    connectedIndex = 0;
                    dataIndex = 0;
                }

            // Updating local database according to data from admin app.
            } else if (connectedIndex == 2) {

                // 0 = request updating data, beginning of connection; 1 = new client; 2 = client data update; 25 = end connection;
                if (dataIndex == 0) {
                    dataIndex = Integer.parseInt(input);
                    if (dataIndex==25) {
                        dataIndex = 0;
                        state = CONNECTED;
                        connectedIndex = 0;
                    }
                    output = SUCCESS;

                    // New client
                } else if (dataIndex == 1) {
                    try {
                        Person temp = new Person(Person.getPersonFromCodedString(input));
                        if(temp.getLoginData().getPassword() == null || temp.getLoginData().getPassword().equals("") || temp.getLoginData().getPassword().length() == 0) {
                            temp.getLoginData().setPassword(temp.getPersonIDNumber());
                        }
                        while (temp.getLoginData().getPassword().length()<6) {
                            temp.getLoginData().setPassword( temp.getLoginData().getPassword().concat(temp.getPersonIDNumber()));
                        }
                        Server.getClientDatabase().add(temp);
                    } catch (Exception e) {
                        dataIndex = 0;
                        state = CONNECTED;
                        connectedIndex = 0;
                        return FAILURE;
                    }
                    output = SUCCESS;
                    dataIndex = 0;

                // Updating client data
                } else if (dataIndex == 2) {
                    try {
                        Server.getClientDatabase().getClientByID(Person.getPersonFromCodedString(input).getLoginData().getPersonID()).updatePerson(Person.getPersonFromCodedString(input));
                    } catch (Exception e) {
                        dataIndex = 0;
                        state = CONNECTED;
                        connectedIndex = 0;
                        return FAILURE;
                    }
                    output = SUCCESS;
                    dataIndex = 0;

                }
            } else if (connectedIndex == 3) {
                admin = null;
                state = WAITING;
                output = SUCCESS;
                dataIndex = 0;
                connectedIndex = 0;
            } else if (connectedIndex == 25) {
                admin = null;
                adminHandler.closeConnection();
                state = WAITING;
                output = SUCCESS;
                dataIndex = 0;
                connectedIndex = 0;
            }

        }
        return output;
    }


}
