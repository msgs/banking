package pl.msgs.server.exception;

public class CommandException extends Exception {
    private String message;

    public CommandException (int errorNumber){
        switch(errorNumber){
            case 101:
                message = "Incorrect command! Error: 101 Unknown option.";
                break;
            case 102:
                message = "Incorrect command! Error: 102 Too much options.";
                break;
            case 103:
                message = "Incomplete command!";
                break;
            case 104:
                message = "Incomplete command! Error: 104 To check information about this command, you should use \"show --help\"";
                break;
            case 105:
                message = "Incorrect command! Error: 105 To check information about this command, you should use \"show --help\"";
                break;
            case 106:
                message = "Incorrect command!";
                break;
            case 107:
                message = "Incomplete command! Error: 104 To check information about this command, you should use \"add --help\"";
                break;
            case 108:
                message = "Incorrect command! Error: 105 To check information about this command, you should use \"add --help\"";
                break;
            case 109:
                message = "Incomplete command! Error: 104 To check information about this command, you should use \"delete --help\"";
                break;
            case 110:
                message = "Incorrect command! Error: 105 To check information about this command, you should use \"delete --help\"";
                break;
            case 111:
                message = "Administrator was not delete.";
                break;
            case 112:
                message = "Error: 112 Problem with email client";
                break;
            case 113:
                message = "Incorrect command! Error: 113 To check information about this command, you should use \"info -send\"";
                break;
            case 114:
                message = "Incomplete command! Error: 104 To check information about this command, you should use \"start --help\"";
                break;
            case 115:
                message = "Incorrect command! Error: 105 To check information about this command, you should use \"start --help\"";
                break;
            default:
                message = "Error! Unknown error number!";
                break;
        }
    }

    @Override
    public String getMessage() {
        return message;
    }
}
