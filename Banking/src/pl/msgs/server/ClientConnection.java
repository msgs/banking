package pl.msgs.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * The class that creates server socket connections waiting for client host connections on port defined by the port variable.
 */
public class ClientConnection implements Runnable {
    private volatile boolean running = true;
    private Thread thread;
    private final int port = 4646;
    private volatile int clientCount = 0;
    private Logger logger = Logger.getLogger(ClientConnection.class.getName());

    /**
     * Starts the server connection.
     */
    public void startConnection() {
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Close the client-server connection.
     * Decreases the client count number.
     */
    public void closeClientConnection() {
        logger.info("Thread has deleted a client handling thread no: " + clientCount);
        clientCount--;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (running) {
                Socket incoming = serverSocket.accept();
                logger.info("Thread has spawning new client handling thread no " + ++clientCount);
                ClientHandler clientHandler = new ClientHandler(incoming, this);
                clientHandler.startConnection();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("End of client connection thread.");

    }

    public int getClientCount() {
        return clientCount;
    }
}