package pl.msgs.server;

import pl.msgs.FTConnection.*;
import pl.msgs.model.*;
import pl.msgs.model.exception.NotEnoughMoneyException;
import pl.msgs.model.exception.TransferValueException;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Main banking server application (may fully run as a background task).
 * Everything connected with bank management happens here.
 */
public class Server {
    // An array that contains every administrator of the bank.
    private static final AdministratorDatabase adminDatabase = new AdministratorDatabase();
    // An array that contains every client of the bank.
    private static final ClientDatabase clientDatabase = new ClientDatabase();
    // While true the server is running.
    private static volatile boolean running = true;
    private static final String keysPath = "./main/keys";
    private static final String historyPath = "./main/history";

    private static FTServices ftServices;
    private static volatile ListTodayOrders ordersList;

    private static String time;
    private static LocalDate today;
    private static LocalTime localTime;
    private static Logger logger = Logger.getLogger(Server.class.getName());

    public static void main(String args[]) {
        logger.info("Bank system is starting...");
        // Server application started.

        //Time when server started
        localTime = LocalTime.now();
        today = LocalDate.now();
        time = "Time: " + localTime + " Date: " + today.getDayOfMonth() + "." + today.getMonth() + "." + today.getYear();

        // Administrator database is loading...
        logger.info("Administrator database is loading...");
        adminDatabase.loadDataFromFile("./main/adminDatabase.dat");
        AdministratorDigitalKey.setPathToFolder(keysPath);
        /*/
        Administrator tempAdmin = new Administrator("First", "Administrator", "+48 685479254", "1234567890123456789012345");
        adminDatabase.add(tempAdmin);

        try {
            new AdministratorDigitalKey(tempAdmin, today, localTime, time);
        } catch (IOException e) {
            logger.warning("Could not create administrator key.\n" + e.toString());
        }
        /*/
        logger.info("Administrator database is successfully loaded.");

        // Client database is loading...
        logger.info("Client database is loading...");
        clientDatabase.loadDataFromFile("./main/clientDatabase.dat");
        /*/
        clientDatabase.add(new Person("Will", "Smith", 1, 1, 1990, "Bow", "4", "2C", "WC2E 9RA", "London", "England", "+44 348429581", "41041617800", "123456"));
        clientDatabase.add(new Person("Jack", "Jones", 2, 2, 1991, "Kapucyńska", "6", "12", "31-113", "Kraków", "Poland", "+48 675139846", "51101616449", "qwerty"));
        clientDatabase.add(new Person("Karen", "Kimura", 3, 3, 1992, "Saint-Roch", "2", "16", "75001", "Paris", "France", "+33 385571396", "93061908770", "abcdef"));

        try {
            clientDatabase.get(0).getAccounts().add(AccountFactory.getAccountWithBalance("standard", clientDatabase.get(0), 10500.0, 0));
            clientDatabase.get(0).getAccounts().add(AccountFactory.getAccountWithBalance("placing", clientDatabase.get(0), 25000.0, 5));
            clientDatabase.get(0).getAccounts().add(AccountFactory.getAccountWithBalance("credit", clientDatabase.get(0), 5000.0, 3));

            clientDatabase.get(1).getAccounts().add(AccountFactory.getAccountWithBalance("standard", clientDatabase.get(1), 15.89, 0));
            clientDatabase.get(1).getAccounts().add(AccountFactory.getAccountWithBalance("standard", clientDatabase.get(1), 128057.0, 0));
            clientDatabase.get(1).getAccounts().add(AccountFactory.getAccountWithBalance("placing", clientDatabase.get(1), 269348.50, 6));

            clientDatabase.get(2).getAccounts().add(AccountFactory.getAccountWithBalance("credit", clientDatabase.get(2), 25000.0, 4));
        } catch (AccountException e) {
            logger.warning(e.toString());
        }
        /*/
        logger.info("Client database is successfully loaded.");


        // Starting local socket connection. Service is going to be available soon.
        logger.info("Local service is turning on...");
        UserConnection userConnection = new UserConnection();
        userConnection.startConnection();
        logger.info("Local service is running.");

        // Starting socket connections. Services are going to be online soon.
        logger.info("Online services are turning on...");
        // Administrators connections.
        AdminConnection adminConnection = new AdminConnection();
        adminConnection.startConnection();
        // Clients connections.
        ClientConnection clientConnection = new ClientConnection();
        clientConnection.startConnection();
        logger.info("Online services are running.");

        //Time when server started
        localTime = LocalTime.now();
        today = LocalDate.now();
        time = "Time: " + localTime + " Date: " + today.getDayOfMonth() + "." + today.getMonth() + "." + today.getYear();




        //System is ready and working until userConnection change the running value to false.
        logger.info("System is ready.");
        while (running) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                logger.warning(e.toString());
            }
        }



        // Closing server app.
        logger.info("System is saving administrator database...");
        if (adminDatabase.saveDataToFile("./main/adminDatabase.dat"))
            logger.info("System saved administrator database");
        else
            logger.severe("System does not saved administrator database...");

        logger.info("System is saving client database...");
        if (clientDatabase.saveDataToFile("./main/clientDatabase.dat"))
            logger.info("System saved client database");
        else
            logger.severe("System does not saved client database...");

        logger.info("System is shutting down...");

        logger.info("Goodbye");
        System.exit(0);
    }

    public static ArrayList<HistoryEvent> getAccountHistory(Account account) {
        ArrayList<HistoryEvent> historyClient = new ArrayList<HistoryEvent>();

        String path = historyPath + "/" + account.getAccountNumber();
        try {
            File file = new File(path);
            if (file.exists()) {
                HistoryEvent historyEvent;
                ObjectInput in = new ObjectInputStream(new FileInputStream(file));
                boolean reading = true;
                while (reading) {
                    try {
                        historyEvent = (HistoryEvent) in.readObject();
                        historyClient.add(historyEvent);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (EOFException e) {
                        reading = false;
                    }
                }
                in.close();
            }
        } catch (
                EOFException e)

        {
            // Just end of file.
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }

        return historyClient;
    }

    /**
     * Makes transfer from account to destination account
     *
     * @param accountSending     .
     * @param destinationAddress .
     * @param value              .
     * @param title              .
     * @param comment            .
     * @return 1 if successful, 2 value 0, 3 if not enough money.
     */
    public static int makeTransfer(Account accountSending, String destinationAddress, Double value, String title, String comment) {
        if (Account.getAccountByNumber(destinationAddress) != null) {
            try {
                accountSending.transfer(value, "transfer", Account.getAccountByNumber(destinationAddress), title, comment);
            } catch (TransferValueException e) {
                return 2;
            } catch (NotEnoughMoneyException e) {
                return 3;
            }
        } else {
            try {
                accountSending.transfer(value, "transfer", destinationAddress, title, comment);
            } catch (TransferValueException e) {
                return 2;
            } catch (NotEnoughMoneyException e) {
                return 3;
            }
        }
        return 1;
    }

    /**
     * Returns administrator database.
     *
     * @return AdministratorDatabase
     */
    public static AdministratorDatabase getAdminDatabase() {
        return adminDatabase;
    }

    /**
     * Returns client database.
     *
     * @return ClientDatabase
     */
    public static ClientDatabase getClientDatabase() {
        return clientDatabase;
    }

    /**
     * After calling this function the server will be stopped soon.
     */
    public static void stopServer() {
        running = false;
    }

    public static String getKeysPath() {
        return keysPath;
    }

    public static String getTime() {
        return time;
    }

    /**
     * Functions to mange connection and receiving data form Ft system
     *
     */
    public static void startFtConnection(int time){
        ftServices = new FTServices(true, time);
        ftServices.connect();
    }

    public static void stopFtConnection(){
        ftServices.endConnection();
    }

    public static ListTodayOrders getOrdersList() {
        return ordersList;
    }

    //list of orders which should be calculates in bank system
    public static void setOrdersList(ListTodayOrders ordersList) {
        Server.ordersList = ordersList;
        int counter;
        for (int i =0; i<clientDatabase.size(); i++){
            for (int j= 0; j < clientDatabase.get(i).getAccounts().size(); j++){
                for (int k = 0; k < ordersList.size(); k++){
                    if(clientDatabase.get(i).getAccounts().get(j).getAccountNumber().equals(ordersList.get(k).getPayerAccountNo())){
                        ListTodayOrders temp = clientDatabase.get(i).getAccounts().get(j).getWaitingTransactions();
                        counter = 0;
                        for (int h = 0; h < temp.size(); h++) {
                            if (temp.get(h).getId()== ordersList.get(k).getId()) {
                                counter++;
                            }
                        }
                        if(counter == 0){
                            temp.add(ordersList.get(k));
                            clientDatabase.get(i).getAccounts().get(j).setWaitingTransactions(temp);
                        }
                    }
                }
            }
        }
         /*
        ListTodayOrders listTodayOrders = TransfersFT.getListConfirmations();
        for (ReadyTransfer rt : ordersList){
            rt.setApproved(true);
            listTodayOrders.add(rt);

        }
        */

    }
}