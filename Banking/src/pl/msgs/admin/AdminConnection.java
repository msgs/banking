package pl.msgs.admin;

import pl.msgs.model.Person;
import pl.msgs.util.StringToArray;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * The class that allows us to connect to the server via socket connection.
 */
public class AdminConnection {
    private final int port;
    private static String host = "77.55.208.177";     // "77.55.218.158" || "localhost"
    private Socket socket;
    private InputStream inStream;
    private OutputStream outStream;
    private Scanner in;
    private PrintWriter out;
    private AdminApp adminApp;
    private boolean connectionState;

    // String containing end of transfer message;
    final String endSignal = "*END!OF!DATABASE*";

    public AdminConnection(int port, AdminApp adminApp) throws IOException {
        this.port = port;
        this.adminApp = adminApp;
        socket = new Socket(host, this.port);
        inStream = socket.getInputStream();
        outStream = socket.getOutputStream();
        in = new Scanner(inStream, "UTF-16");
        out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);
        connectionState = false;

    }

    // After .checkForAdmin() method contains indexes of password for authorization.
    private int[] indexes;

    /**
     * Checks if the administrator is in the database.
     * Shows indexes of password symbols needed to be passed to server for authorization - int[] indexes.
     *
     * @param adminID
     * @return true when administrator exist in the database.
     */
    public boolean checkForAdmin(int adminID) {
        // If server is ready.
        out.println("1");

        int input = Integer.parseInt(in.nextLine());
        if (input == 1) {
            //Server ready. Ask for admin.
            out.println(adminID);
            input = Integer.parseInt(in.nextLine());
            if (input == 1) {
                // Fill indexes array.
                out.println("1");
                indexes = StringToArray.stringToIntArray(in.nextLine(), ",");
                return true;
            }
        }
        return false;
    }

    /**
     * Authorizes administrator password with server by sending required symbols of password.
     *
     * @param symbols required symbols of password.
     * @return true when password is authorized.
     */
    public boolean authorizeAdminPassword(char[] symbols) {
        // Send server an array of char contains password symbols at indexes.
        out.println(Arrays.toString(symbols));
        int input = Integer.parseInt(in.nextLine());
        if (input == 1) {
            return true;
        }
        return false;
    }

    /**
     * Authorizes administrator key with server.
     *
     * @param key read from a key file.
     * @return true when a key is valid.
     */
    public boolean authorizeAdminConnection(String key) {
        // Send server a key read from a key file.
        out.println(key);
        int input = Integer.parseInt(in.nextLine());
        if (input == 1) {
            connectionState = true;
            return true;
        }
        return false;
    }

    /**
     * Refreshes local client database.
     */
    public void refreshLocalClientDatabase() throws IOException{
        adminApp.getClientDatabase().clear();
        // Request client data.
        out.println("1");
        if(Integer.parseInt(in.nextLine())!=1) {
            throw new IOException("Could not receive data");
        }

        // Request end signal string.
        out.println("1");
        // Get end transmission string.
        String endSignal = in.nextLine();
        boolean receiving = true;

        do {
            out.println("2");
            String received = in.nextLine();
            if (received.equals(endSignal)) {
                receiving = false;
            } else if (received.equals("ERROR")){
                throw new IOException("Could not receive data");
            } else {
                try {
                    adminApp.getClientDatabase().add(Person.getPersonFromCodedString(received));
                } catch (Exception e) {
                    out.println("3");
                    throw new IOException("Could not receive data");
                }
            }
        } while (receiving);
    }

    /**
     * Logout the client.
     *
     * @return true when logout was successful.
     */
    public boolean logout() {
        connectionState = false;
        out.println("3");
        if (Integer.parseInt(in.nextLine()) == 1) {
            out.println("1");
            if (Integer.parseInt(in.nextLine()) == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Logout the client and sends a message to the server that the connection wil be close soon.
     *
     * @return true when logout was successful.
     */
    public boolean exitApp() {
        if (connectionState) {
            out.println("25");
            if (Integer.parseInt(in.nextLine()) == 1) {
                out.println("1");
                if (Integer.parseInt(in.nextLine()) == 1) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public void pushNewData(ArrayList<Integer> changedClient, ArrayList<Integer> chengeType) throws IOException {
        // Request client database updating.
        out.println("2");
        if(Integer.parseInt(in.nextLine())!=1) {
            throw new IOException("Could not receive data");
        }

        for (int i = adminApp.getClientDatabaseChanged().size() - 1; i >= 0 ; i--) {

            // Send type of change
            out.println(adminApp.getClientChangeType().get(i));
            if(Integer.parseInt(in.nextLine())!=1) {
                throw new IOException("Could not receive data");
            }
            // Send data according to change type
            if (adminApp.getClientChangeType().get(i) == 1) {
                Person temp = adminApp.getClientDatabase().get(adminApp.getClientDatabaseChanged().get(i));
                out.println(temp.personToCodedString());
            } else if (adminApp.getClientChangeType().get(i) == 2) {
                Person temp = adminApp.getClientDatabase().get(adminApp.getClientDatabaseChanged().get(i));
                out.println(temp.personToCodedString());
            }
            // Delete next updated index if succeeded.
            if(Integer.parseInt(in.nextLine())!=1) {
                throw new IOException("Could not receive data");
            } else {
                adminApp.getClientDatabaseChanged().remove(i);
                adminApp.getClientChangeType().remove(i);
            }

        }
        // Close updating
        out.println(25);
        if(Integer.parseInt(in.nextLine())!=1) {
            throw new IOException("Could not receive data");
        }
    }


    // Getter
    public int[] getIndexes() {
        return indexes;
    }
}
