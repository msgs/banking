package pl.msgs.admin.view;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.msgs.admin.AdminApp;
import pl.msgs.model.Person;
import pl.msgs.util.DateToString;

/**
 * Dialog to edit details of a person.
 *
 * @author Mikołaj Gwiazdowicz
 */
public class ClientEditDialogController {
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField personIDTextField;
    @FXML
    private TextField birthdayField;
    @FXML
    private TextField phoneNumberTextField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField buildingTextField;
    @FXML
    private TextField flatTextField;
    @FXML
    private TextField postalCodeField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField countryTextField;

    private AdminApp adminApp;
    private Stage dialogStage;
    private Person person;
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person
     */
    public void setPerson(Person person) {
        this.person = person;

        firstNameField.setText(person.getFirstName());
        lastNameField.setText(person.getLastName());
        personIDTextField.setText(person.getPersonIDNumber());
        birthdayField.setText(DateToString.format(person.getBirthdayDate()));
        birthdayField.setPromptText("DD.MM.YYYY");
        phoneNumberTextField.setText(person.getPhoneNumber());
        streetField.setText(person.getHomeAddress().getStreet());
        buildingTextField.setText(person.getHomeAddress().getBuilding());
        flatTextField.setText(person.getHomeAddress().getFlat());
        postalCodeField.setText(person.getHomeAddress().getZipCode());
        cityField.setText(person.getHomeAddress().getCity());
        countryTextField.setText(person.getHomeAddress().getCountry());
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return isOkClicked value
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            person.setFirstName(firstNameField.getText());
            person.setLastName(lastNameField.getText());
            person.setPersonIDNumber(personIDTextField.getText());
            person.setBirthdayDate(DateToString.parse(birthdayField.getText()));
            person.setPhoneNumber(phoneNumberTextField.getText());
            person.getHomeAddress().setStreet(streetField.getText());
            person.getHomeAddress().setBuilding(buildingTextField.getText());
            person.getHomeAddress().setFlat(flatTextField.equals("") || flatTextField.equals(null) ? null : flatTextField.getText());
            person.getHomeAddress().setZipCode(postalCodeField.getText());
            person.getHomeAddress().setCity(cityField.getText());
            person.getHomeAddress().setCountry(countryTextField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (firstNameField.getText() == null || firstNameField.getText().length() == 0) {
            errorMessage += "No valid first name!\n";
        }
        if (lastNameField.getText() == null || lastNameField.getText().length() == 0) {
            errorMessage += "No valid last name!\n";
        }
        if (personIDTextField.getText() == null || personIDTextField.getText().length() == 0) {
            errorMessage += "No valid person ID!\n";
        }
        if (birthdayField.getText() == null || birthdayField.getText().length() == 0) {
            errorMessage += "No valid birthday!\n";
        } else {
            if (!DateToString.validDate(birthdayField.getText())) {
                errorMessage += "No valid birthday! Use the format DD.MM.YYYY\n";
            }
        }
        if (phoneNumberTextField.getText() == null || phoneNumberTextField.getText().length() == 0) {
            errorMessage += "No valid phone number!\n";
        }
        if (streetField.getText() == null || streetField.getText().length() == 0) {
            errorMessage += "No valid street!\n";
        }
        if (postalCodeField.getText() == null || postalCodeField.getText().length() == 0) {
            errorMessage += "No valid postal code!\n";
        }
        if (cityField.getText() == null || cityField.getText().length() == 0) {
            errorMessage += "No valid city!\n";
        }
        if (countryTextField.getText() == null || countryTextField.getText().length() == 0) {
            errorMessage += "No valid country!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            //show error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
