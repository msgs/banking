package pl.msgs.admin.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import pl.msgs.admin.AdminApp;

import java.io.IOException;
import java.util.ArrayList;

public class RootAdminLayoutController {

    private AdminApp adminApp;

    /**
     * Handles Refresh database button in bank menu panel.
     */
    @FXML
    private void handleRefreshDatabase() {
        try {
            adminApp.getAdminConnection().refreshLocalClientDatabase();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(adminApp.getPrimaryStage());
            alert.setTitle("Banking application information");
            alert.setHeaderText("Client database refreshed.");
            alert.setContentText("");
            alert.showAndWait();
        } catch (IOException e) {
            adminApp.showRefreshError();
        }
    }

    /**
     * Handles Push and Refresh database button in bank menu panel.
     */
    @FXML
    private void handlePushRefreshDatabase() {
        try {
            pushChanges(adminApp.getClientDatabaseChanged(), adminApp.getClientChangeType());

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(adminApp.getPrimaryStage());
            alert.setTitle("Banking application information");
            alert.setHeaderText("Client database pushed.");
            alert.setContentText("");
            alert.showAndWait();
        } catch (IOException e) {
            adminApp.showPushError();
        }

        handleRefreshDatabase();
    }

    /**
     * Handles About button in bank menu panel.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(adminApp.getPrimaryStage());
        alert.setTitle("Banking application information");
        alert.setHeaderText("About");
        alert.setContentText("Application to create and manage clients data in bank.\n\nAdministrator application made by Mikołaj Gwiazdowicz.");
        alert.showAndWait();
    }

    /**
     * Handles logout button in bank menu panel.
     */
    @FXML
    private void handleLogout() {
        adminApp.getAdminConnection().logout();
        adminApp.showLoginPanel();
    }

    /**
     * Handles Exit and Push button in bank menu panel.
     */
    @FXML
    private void handlePushClose() {
        try {
            pushChanges(adminApp.getClientDatabaseChanged(), adminApp.getClientChangeType());

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(adminApp.getPrimaryStage());
            alert.setTitle("Banking application information");
            alert.setHeaderText("Client database pushed.");
            alert.setContentText("");
            alert.showAndWait();

            adminApp.getPrimaryStage().close();
        } catch (IOException e) {
            adminApp.showPushError();
        }

    }

    /**
     * Handles Exit button in bank menu panel.
     */
    @FXML
    private void handleClose() {
        adminApp.getPrimaryStage().close();
    }

    /**
     * Pushes data to update server database.
     * @param changedClient
     * @param chengeType
     * @throws IOException
     */
    private void pushChanges(ArrayList<Integer> changedClient, ArrayList<Integer> chengeType) throws IOException {
        adminApp.getAdminConnection().pushNewData(changedClient, chengeType);
    }

    /**
     * Is called by the client application to give a reference back to itself.
     *
     * @param adminApp reference
     */
    public void setAdminApp(AdminApp adminApp) {
        this.adminApp = adminApp;
    }
}
