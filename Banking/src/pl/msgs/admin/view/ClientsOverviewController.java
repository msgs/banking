package pl.msgs.admin.view;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import pl.msgs.admin.AdminApp;
import pl.msgs.model.Account;
import pl.msgs.model.Person;
import pl.msgs.util.DateToString;

import java.io.IOException;
import java.util.ArrayList;

public class ClientsOverviewController {
    @FXML
    private TableView<Person> clientTable;
    @FXML
    private TableColumn<Person, String> firstNameColumn;
    @FXML
    private TableColumn<Person, String> lastNameColumn;
    @FXML
    private TableColumn<Person, Integer> idColumn;
    @FXML
    private Button newButton;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;
    @FXML
    private VBox accountsBox;

    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label clientIDLabel;
    @FXML
    private Label idLabel;
    @FXML
    private Label birthdayLabel;
    @FXML
    private Label phoneNumberLabel;
    @FXML
    private Label streetLabel;
    @FXML
    private Label buildingFlatLabel;
    @FXML
    private Label zipCodeLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label countryLabel;

    // reference to the client application.
    private AdminApp adminApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public ClientsOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the client table with three columns.
        firstNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirstName()));
        lastNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLastName()));
        idColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getLoginData().getPersonID()).asObject());

        // Clear client details.
        showPersonDetails(null);

        // Listen for selection changes and show the client details when changed.
        clientTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            showPersonDetails(newValue);
            showAccountsOverview(newValue == null ? null : newValue.getAccounts());
            if (newValue != null) {
                newButton.setDisable(false);
                if (newValue.getLoginData().getPersonID() == 0) {
                    deleteButton.setDisable(false);
                    editButton.setDisable(true);
                } else {
                    deleteButton.setDisable(true);
                    editButton.setDisable(false);
                }
            } else {
                newButton.setDisable(true);
                editButton.setDisable(true);
            }

        });
    }

        /**
         * Fills all text fields to show details about the client.
         * If the specified client is null, all text fields are cleared.
         *
         * @param person the person or null
         */

    private void showPersonDetails(Person person) {
        if (person != null) {
            // Fill the labels with info from the person object.
            firstNameLabel.setText(person.getFirstName());
            lastNameLabel.setText(person.getLastName());
            clientIDLabel.setText(person.getPersonIDNumber());
            idLabel.setText(Integer.toString(person.getLoginData().getPersonID()));
            birthdayLabel.setText(DateToString.format(person.getBirthdayDate()));
            phoneNumberLabel.setText(person.getPhoneNumber());
            streetLabel.setText(person.getHomeAddress().getStreet());
            buildingFlatLabel.setText(person.getHomeAddress().getFlat().equals(null) || person.getHomeAddress().getFlat().equals("") ? person.getHomeAddress().getBuilding() : person.getHomeAddress().getBuilding() + "/" + person.getHomeAddress().getFlat());
            zipCodeLabel.setText(person.getHomeAddress().getZipCode());
            cityLabel.setText(person.getHomeAddress().getCity());
            countryLabel.setText(person.getHomeAddress().getCountry());
        } else {
            // Person is null, remove all the text.
            firstNameLabel.setText("");
            lastNameLabel.setText("");
            clientIDLabel.setText("");
            idLabel.setText("");
            birthdayLabel.setText("");
            phoneNumberLabel.setText("");
            streetLabel.setText("");
            buildingFlatLabel.setText("");
            zipCodeLabel.setText("");
            cityLabel.setText("");
            countryLabel.setText("");
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit details for a new person.
     */
    @FXML
    private void handleNewClient() {
        Person tempPerson = new Person("","",1,1,2000,"","","","","","","","","",true);
        boolean okClicked = adminApp.showClientEditDialog(tempPerson);
        if (okClicked) {
            adminApp.getClientDatabaseChanged().add(adminApp.getClientDatabase().size());
            adminApp.getClientChangeType().add(1);
            adminApp.getClientDatabase().add(tempPerson);
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit details for a new person.
     */
    @FXML
    private void handleEditClient() {
        Person selectedPerson = clientTable.getSelectionModel().getSelectedItem();
        boolean okClicked = adminApp.showClientEditDialog(selectedPerson);
        if (okClicked) {
            adminApp.getClientDatabaseChanged().add(clientTable.getSelectionModel().getFocusedIndex());
            adminApp.getClientChangeType().add(2);
        }
    }

    /**
     * Called when the user clicks the delete button.
     */
    @FXML
    private void handleDeleteClient() {
        int selectedIndex = clientTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0 && clientTable.getSelectionModel().getSelectedItem().getLoginData().getPersonID() == 0) {
            adminApp.getClientDatabaseChanged().remove(selectedIndex);
            adminApp.getClientChangeType().remove(selectedIndex);
            clientTable.getItems().remove(selectedIndex);
        }
    }

    /**
     * Is called by the client application to give a reference back to itself.
     *
     * @param adminApp reference
     */
    public void setAdminApp(AdminApp adminApp) {
        this.adminApp = adminApp;

        // Add client database to the table
        clientTable.setItems(adminApp.getClientDatabase());
    }

    /**
     * Shows overview of client accounts.
     * @param accounts array of account.
     */
    private void showAccountsOverview(ArrayList<Account> accounts) {
        accountsBox.getChildren().clear();
        if(accounts != null) {
            for (int i = 0; i < accounts.size(); i++) {
                try {
                    // Loads the account overview.
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(AdminApp.class.getResource("view/AccountOverview.fxml"));
                    GridPane accountOverview = (GridPane) loader.load();

                    // Give the controller required data.
                    AccountOverviewController controller = loader.getController();
                    controller.showAccountOverview(accounts.get(i));

                    // Add new account box to the account container.
                    accountsBox.getChildren().add(accountOverview);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
