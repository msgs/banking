package pl.msgs.admin.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import pl.msgs.model.Account;
import pl.msgs.model.CreditAccount;
import pl.msgs.model.Placing;

public class AccountOverviewController {
    @FXML
    private Label accountNumberLabel;
    @FXML
    private Label balanceLabel;
    @FXML
    private Label costLabel;
    @FXML
    private Label cardsQuantityLabel;
    @FXML
    private Label creditJustLabel;
    @FXML
    private Label creditLabel;
    @FXML
    private Circle circle;

    public void showAccountOverview(Account account) {
        accountNumberLabel.setText(account.getAccountNumber());
        balanceLabel.setText(String.format("%.2f PLN", account.getBalance()));
        costLabel.setText(String.format("%.2f PLN", account.getCost()));
        cardsQuantityLabel.setText(Integer.toString(account.getCards().size()));
        if(account instanceof CreditAccount) {
            creditJustLabel.setVisible(true);
            creditLabel.setVisible(true);
            creditLabel.setText(String.format("%.2f PLN", ((CreditAccount) account).getCredit()));
            circle.setFill(Color.web("ff8e00"));
        } else if(account instanceof Placing) {
            circle.setFill(Color.web("3ad100"));
        } else {
            circle.setFill(Color.web("0077FF"));
        }
    }
}
