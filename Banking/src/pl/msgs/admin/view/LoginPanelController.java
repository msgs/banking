package pl.msgs.admin.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import pl.msgs.admin.AdminApp;
import pl.msgs.model.AdministratorDigitalKey;

import java.io.File;
import java.util.ArrayList;

public class LoginPanelController {

    @FXML
    private TextField adminIDTextField;
    @FXML
    private Button validateIDButton;
    @FXML
    private Button validatePasswordButton;
    @FXML
    private Button loginButton;

    // All password TextFields.
    @FXML
    private PasswordField pTF1;
    @FXML
    private PasswordField pTF2;
    @FXML
    private PasswordField pTF3;
    @FXML
    private PasswordField pTF4;
    @FXML
    private PasswordField pTF5;
    @FXML
    private PasswordField pTF6;
    @FXML
    private PasswordField pTF7;
    @FXML
    private PasswordField pTF8;
    @FXML
    private PasswordField pTF9;
    @FXML
    private PasswordField pTF10;
    @FXML
    private PasswordField pTF11;
    @FXML
    private PasswordField pTF12;
    @FXML
    private PasswordField pTF13;
    @FXML
    private PasswordField pTF14;
    @FXML
    private PasswordField pTF15;
    @FXML
    private PasswordField pTF16;
    @FXML
    private PasswordField pTF17;
    @FXML
    private PasswordField pTF18;
    @FXML
    private PasswordField pTF19;
    @FXML
    private PasswordField pTF20;
    @FXML
    private PasswordField pTF21;
    @FXML
    private PasswordField pTF22;
    @FXML
    private PasswordField pTF23;
    @FXML
    private PasswordField pTF24;
    @FXML
    private PasswordField pTF25;

    // ArrayList containing all password TextFields.
    private ArrayList<PasswordField> passwordTextFieldArray = new ArrayList<PasswordField>();

    // reference to the client application.
    private AdminApp adminApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public LoginPanelController() {
    }

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Adding all password TextFields to an ArrayList.
        passwordTextFieldArray.add(pTF1);
        passwordTextFieldArray.add(pTF2);
        passwordTextFieldArray.add(pTF3);
        passwordTextFieldArray.add(pTF4);
        passwordTextFieldArray.add(pTF5);
        passwordTextFieldArray.add(pTF6);
        passwordTextFieldArray.add(pTF7);
        passwordTextFieldArray.add(pTF8);
        passwordTextFieldArray.add(pTF9);
        passwordTextFieldArray.add(pTF10);
        passwordTextFieldArray.add(pTF11);
        passwordTextFieldArray.add(pTF12);
        passwordTextFieldArray.add(pTF13);
        passwordTextFieldArray.add(pTF14);
        passwordTextFieldArray.add(pTF15);
        passwordTextFieldArray.add(pTF16);
        passwordTextFieldArray.add(pTF17);
        passwordTextFieldArray.add(pTF18);
        passwordTextFieldArray.add(pTF19);
        passwordTextFieldArray.add(pTF20);
        passwordTextFieldArray.add(pTF21);
        passwordTextFieldArray.add(pTF22);
        passwordTextFieldArray.add(pTF23);
        passwordTextFieldArray.add(pTF24);
        passwordTextFieldArray.add(pTF25);
        loginButton.setDisable(true);
    }

    /**
     * Is called by the administrator application to give a reference back to itself.
     *
     * @param adminApp
     */
    public void setAdminApp(AdminApp adminApp) {
        this.adminApp = adminApp;
    }

    /**
     * Handles first part of logging process.
     * Checks admin ID if is valid.
     */
    @FXML
    private void handleValidateIDButton() {
        if (!adminIDTextField.getText().equals("")) {
            try {
                int adminID = Integer.parseInt(adminIDTextField.getText());

                if (adminApp.getAdminConnection().checkForAdmin(adminID)) {
                    adminIDTextField.setDisable(true);
                    validateIDButton.setVisible(false);
                    validateIDButton.setDefaultButton(false);
                    validatePasswordButton.setVisible(true);
                    validatePasswordButton.setDefaultButton(true);
                    enableByIndex(adminApp.getAdminConnection().getIndexes());
                } else {
                    adminIDTextField.setText("");
                    adminIDTextField.setPromptText("Invalid ID");
                }
            } catch (NumberFormatException e) {
                adminIDTextField.setText("");
                adminIDTextField.setPromptText("ID is a set of number");
            }
        }
    }

    /**
     * Handles second part of logging process.
     * Checks admin password if is valid.
     */
    @FXML
    private void handleValidatePasswordButton() {
        if (checkPasswordByIndex(adminApp.getAdminConnection().getIndexes())) {
            char[] symbols = getPasswordAsArray(adminApp.getAdminConnection().getIndexes());
            if (adminApp.getAdminConnection().authorizeAdminPassword(symbols)) {
                disablePasswordButtons();
                validatePasswordButton.setVisible(false);
                validatePasswordButton.setDefaultButton(false);
                loginButton.setDisable(false);
                loginButton.setVisible(true);
                loginButton.setDefaultButton(true);
            } else {
                disablePasswordButtons();
                adminIDTextField.setDisable(false);
                adminIDTextField.setText("Invalid password");
                validatePasswordButton.setVisible(false);
                validatePasswordButton.setDefaultButton(false);
                validateIDButton.setVisible(true);
                validateIDButton.setDefaultButton(true);
            }
        }
    }

    /**
     * Handles third part of logging process.
     * Checks admin key if is valid.
     */
    @FXML
    private void handleLoginButton() throws Exception {
        FileChooser fileChooser = new FileChooser();
        // Show open file dialog
        File file = fileChooser.showOpenDialog(adminApp.getPrimaryStage());

        boolean test = false;
        if (file != null) {
            String adminKey = null;
            adminKey = AdministratorDigitalKey.getStringAdminKeyFromFile(file);
            if (adminApp.getAdminConnection().authorizeAdminConnection(adminKey)) {
                test = true;
            }
        } else {
            adminApp.getAdminConnection().authorizeAdminConnection("foo");
        }
        if (test) {
            adminApp.showClientsOverview();
        } else {
            // Not proper key selected
            loginButton.setVisible(false);
            loginButton.setDefaultButton(false);
            adminIDTextField.setDisable(false);
            adminIDTextField.setText("Invalid key");
            validateIDButton.setVisible(true);
            validateIDButton.setDefaultButton(true);
        }
    }


    /**
     * Disables and clears all password TextFields.
     */
    private void disablePasswordButtons() {
        passwordTextFieldArray.forEach(passwordField -> {
            passwordField.setDisable(true);
            passwordField.setText("");
        });
    }

    /**
     * Enables required password TextFields - specified by indexes.
     *
     * @param indexs
     */
    private void enableByIndex(int[] indexs) {
        KeyEvent event = new KeyEvent(KeyEvent.KEY_PRESSED, KeyCode.TAB.toString(), KeyCode.TAB.toString(), KeyCode.TAB, false, false, false, false);
        for (int i = 0; i < indexs.length; i++) {
            passwordTextFieldArray.get(indexs[i]).setDisable(false);
            final int no = i;
            passwordTextFieldArray.get(indexs[i]).lengthProperty().addListener((observable, oldValue, newValue) -> {
                if ((newValue.intValue() == 1 && oldValue.intValue() == 0) || (newValue.intValue() == 1 && oldValue.intValue() == 1)) {
                    passwordTextFieldArray.get(indexs[no]).fireEvent(event);
                } else if (newValue.intValue() > 1 && oldValue.intValue() == 1) {
                    passwordTextFieldArray.get(indexs[no]).setText(passwordTextFieldArray.get(indexs[no]).getText().substring(passwordTextFieldArray.get(indexs[no]).getText().length() - 1));
                    passwordTextFieldArray.get(indexs[no]).fireEvent(event);
                }
            });
        }
    }

    /**
     * Checks required password TextFields - specified by indexes.
     *
     * @param indexs
     * @return true if required TextFields are not empty.
     */
    private boolean checkPasswordByIndex(int[] indexs) {
        boolean test = true;
        for (int i = 0; i < indexs.length && test; i++) {
            if (passwordTextFieldArray.get(indexs[i]).getText().equals(""))
                test = false;
        }
        return test;
    }

    /**
     * Returns password symbols from specified indexes as a char array.
     *
     * @param indexs
     * @return char[] array of chars at specified password indexes.
     */
    private char[] getPasswordAsArray(int[] indexs) {
        char[] symbols = new char[indexs.length];
        for (int i = 0; i < indexs.length; i++) {
            symbols[i] = passwordTextFieldArray.get(indexs[i]).getText().charAt(0);
        }
        return symbols;
    }

}
