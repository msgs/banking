package pl.msgs.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.msgs.admin.view.ClientEditDialogController;
import pl.msgs.admin.view.ClientsOverviewController;
import pl.msgs.admin.view.LoginPanelController;
import pl.msgs.admin.view.RootAdminLayoutController;
import pl.msgs.model.Person;

/**
 * Main administrator's application for working with server of the bank.
 */
public class AdminApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private AdminConnection adminConnection;
    private int port = 4647;
    private Logger logger = Logger.getLogger(AdminApp.class.getName());
    private ObservableList<Person> clientDatabase = FXCollections.observableArrayList();

    // Contains indexes of changed clients in local database
    private ArrayList<Integer> clientDatabaseChanged = new ArrayList<Integer>();
    // Contains types of changes corresponding to clientDatabaseChanged.
    // 1 - new client, 2 - changed client data.
    private ArrayList<Integer> clientChangeType = new ArrayList<Integer>();

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Banking Application");

        initRootLayout();

        showLoginPanel();
    }

    /**
     * Initializes the root layout.
     */
    private void initRootLayout() {
        try {
            // Load the root layout form the fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AdminApp.class.getResource("view/RootAdminLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            adminConnection = new AdminConnection(port, this);
            primaryStage.setOnCloseRequest(event -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.initOwner(primaryStage);
                alert.setTitle("Banking application closing");
                alert.setHeaderText("Are you sure you want to close banking application");
                if (clientDatabaseChanged.size() > 0) {
                    alert.setContentText("You have "+ clientDatabaseChanged.size() + " not pushed changes.");
                } else {
                    alert.setContentText("");
                }
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.CANCEL) {
                    event.consume();
                }
                exitApp();
            });

            // Give the controller access to the main app.
            RootAdminLayoutController controller = loader.getController();
            controller.setAdminApp(this);
        } catch (IOException e) {
            logger.warning(e.toString());
            showConnectionError();
        }
    }

    /**
     * Shows login panel inside the root layout.
     */
    public void showLoginPanel() {
        try {
            // Load login panel
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AdminApp.class.getResource("view/LoginPanel.fxml"));
            AnchorPane loginPanel = (AnchorPane) loader.load();

            // Show login panel inside the root layout.
            rootLayout.setCenter(loginPanel);

            // Give the controller access to the main app.
            LoginPanelController controller = loader.getController();
            controller.setAdminApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the clients overview inside the root layout.
     */
    public void showClientsOverview() {
        try {
            // Loads the clients overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AdminApp.class.getResource("view/ClientsOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set the clients overview into the center of the root layout.
            rootLayout.setCenter(personOverview);

            // Give the controller access to the main app.
            ClientsOverviewController controller = loader.getController();
            controller.setAdminApp(this);

            rootLayout.getTop().setVisible(true);
            // Load client database
            try {
                adminConnection.refreshLocalClientDatabase();
            } catch (IOException e) {
                showRefreshError();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens a dialog to edit details for the specified person.
     * If the user clicks OK, the changes are saved into the provided person object and true is returned.
     *
     * @param person the person object to be edited
     * @return true if the user clicked Ok, false otherwise.
     */
    public boolean showClientEditDialog(Person person) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AdminApp.class.getResource("view/ClientEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Client");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            ClientEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPerson(person);

            // Show the dialog and wait util the user close it.
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Returns the administrator connection.
     *
     * @return adminConnection.
     */
    public AdminConnection getAdminConnection() {
        return adminConnection;
    }

    /**
     * Returns the main stage.
     *
     * @return primaryStage of admin's view
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Generates alert when application could not connect to the server.
     */
    private void showConnectionError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Banking application error");
        alert.setHeaderText("Could not connect to the server.");
        alert.setContentText("Try again later.\n\nIf this error reappears again please contact with our support team.");
        alert.setOnCloseRequest(event -> {
            exitApp();
            alert.close();
            primaryStage.close();
        });

        alert.showAndWait();
    }

    /**
     * Generates alert when application could not connect to the server and download client database.
     */
    public void showRefreshError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Banking application error");
        alert.setHeaderText("Could not refresh client database.");
        alert.setContentText("Try again later.\n\nIf this error reappears again please contact with our support team.");
        alert.showAndWait();
    }

    /**
     * Generates alert when application could not connect to the server and download client database.
     */
    public void showPushError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Banking application error");
        alert.setHeaderText("Could not push client database.");
        alert.setContentText("Try again later.\n\nIf this error reappears again please contact with our support team.");
        alert.showAndWait();
    }

    /**
     * Close connection with a server and then app can be close.
     */
    private void exitApp() {
        adminConnection.exitApp();
    }

    /**
     * Returns local client database
     * @return ClientDatabase
     */
    public ObservableList<Person> getClientDatabase() {
        return clientDatabase;
    }

    /**
     * Returns arraylist of changed client in database.
     * @return ClientDatabase
     */
    public ArrayList<Integer> getClientDatabaseChanged() {
        return clientDatabaseChanged;
    }

    /**
     * Returns array list of types of changes in client database changed.
     * @return ClientDatabase
     */
    public ArrayList<Integer> getClientChangeType() {
        return clientChangeType;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
