package pl.msgs.model;

import pl.msgs.FTConnection.ListTodayOrders;
import pl.msgs.FTConnection.TransfersFT;
import pl.msgs.model.exception.NotEnoughMoneyException;
import pl.msgs.model.exception.TransferValueException;
import pl.msgs.server.Server;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

// Extends functionality saving/trading accounts?
public class Account implements Serializable {
    // First account number in a bank database.
    private static final String firstAccountNumber = "18002793554325151822000000";
    private static String nextAccountNumber = firstAccountNumber;

    private final String accountNumber;
    private final Person owner;
    private double balance;
    private double cost; //conduct of account cost per month ???
    private ArrayList<Card> cards;
    private static final String HISTORY_PATH = "./main/history";
    private ListTodayOrders waitingTransactions = new ListTodayOrders();
    private ListTodayOrders accomplishedTransactions = new ListTodayOrders();


    // Constructors of this class.
    public Account(Person owner) {
        this.owner = owner;
        this.balance = 0;
        this.cost = 0;
        this.accountNumber = Account.assignNextAccountNumber();
        this.cards = new ArrayList<Card>();
    }

    public Account(Person owner, double balance) {
        this.owner = owner;
        this.balance = balance;
        this.cost = 0;
        this.accountNumber = Account.assignNextAccountNumber();
        this.cards = new ArrayList<Card>();
    }

    /**
     * Transfers specified amount of money to an account specified by destination account number.
     * It generates history event with specific title and comment.
     *
     * @param value                    Value of money to transfer
     * @param type                     Type of transfer: eg. transfer or card
     * @param destinationAccountNumber Destination account number specified by String
     * @param title                    Title of a transfer
     * @param comment                  Comment of a transfer
     * @return true if transfer ends successful.
     */
    public boolean transfer(double value, String type, String destinationAccountNumber, String title, String comment) throws NotEnoughMoneyException, TransferValueException {
        if (value > 0) {
            if (value > balance) {
                throw new NotEnoughMoneyException("You do not have enough money on this account");
            } else if (value == balance) {
                throw new TransferValueException("You cannot make transfer with 0 value.");
            } else if (value < balance) {
                comment = "Transfer of " + String.format("%.2f PLN", value) + " from account: " + this.getAccountNumber() + " to account : " + destinationAccountNumber + ".\n" + comment;
                HistoryEvent event = new HistoryEvent(type, LocalDate.now(), value, title, comment);
                String path = HISTORY_PATH + "/" + accountNumber;
                if (!writeEventToFile(path, event))
                    return false;

                balance -= value;
                return true;
            }
        }
        return false;
    }

    public boolean transfer(double value, String type, Account destinationAccount, String title, String comment) throws TransferValueException, NotEnoughMoneyException {
        if (this.transfer(value, type, destinationAccount.getAccountNumber(), title, comment)) {
            comment = "Transfer of " + String.format("%.2f PLN", value) + " from account: " + this.getAccountNumber() + " to account : " + destinationAccount.accountNumber + ".\n" + comment;
            HistoryEvent event = new HistoryEvent("transfer", LocalDate.now(), value, title, comment);
            Account account = getAccountByNumber(destinationAccount.getAccountNumber());
            if (account == null)
                return false;

            String path = HISTORY_PATH + "/" + account.accountNumber;
            if (!writeEventToFile(path, event))
                return false;

            destinationAccount.addValue(value);
            return true;
        }
        return false;
    }

    /**
     * Adds specific value to account balance.
     *
     * @param value greater than 0.
     */
    public void addValue(double value) {
        if (value > 0) {
            balance += value;
        }
    }

    /**
     * Subtracts specific value from account balance.
     *
     * @param value greater than 0.
     */
    public void subtractValue(double value) {
        if (value > 0) {
            balance -= value;
        }
    }

    // Generates new account number.
    private static String assignNextAccountNumber() {
        String temp = nextAccountNumber;
        int number = Integer.parseInt(nextAccountNumber.substring(20));
        number++;
        nextAccountNumber = nextAccountNumber.substring(0, 20) + String.format("%06d", number);
        return temp;

    }

    // Getters and Setters
    public String getAccountNumber() {
        return accountNumber;
    }

    public Person getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public static String getNextAccountNumber() {
        return nextAccountNumber;
    }

    public static Account getAccountByNumber(String number) {
        Person client;
        ArrayList<Account> accounts;
        for (int i = LoginData.getFirstPersonID(); i <= LoginData.getNextPersonID(); i++) {
            if ((client = Server.getClientDatabase().getClientByID(i)) != null) {
                accounts = client.getAccounts();
                for (int j = 0; j < accounts.size(); j++) {
                    if (accounts.get(j).getAccountNumber().equals(number)) {
                        return accounts.get(j);
                    }
                }
            }
        }
        return null;
    }

    public static String getFirstAccountNumber() {
        return firstAccountNumber;
    }

    // Main method to check functionality.
    public static void main(String[] args) {
        Account account = new Account(null, 500);
        System.out.println("Account owner = " + account.getOwner());
        System.out.println("Account number = " + account.getAccountNumber());
        System.out.println("Account balance = " + account.getBalance());
        System.out.println("Account cost = " + account.getCost());
        account.setBalance(1000);
        account.setCost(200);
        System.out.println("Account balance = " + account.getBalance());
        System.out.println("Account cost = " + account.getCost());

        System.out.println();
        System.out.println("Next new account number = " + Account.getNextAccountNumber());

    }

    private boolean writeEventToFile(String path, HistoryEvent event) {
        try {
            ObjectOutputStream out;
            File file = new File(path);
            if (!file.exists()) {
                if (file.getParentFile() != null)
                    file.getParentFile().mkdirs();  // Creates parent directories if not exists.
                file.createNewFile();
                out = new ObjectOutputStream(new FileOutputStream(file, false));

            } else {
                out = new AppendingObjectOutputStream(new FileOutputStream(file, true));
            }

            try {
                out.writeObject(event);
            } catch (IOException e) {
                e.printStackTrace();
                out.close();
                return false;
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }



     //getters and setters transactions list
    public ListTodayOrders getWaitingTransactions() {
        return waitingTransactions;
    }

    public void setWaitingTransactions(ListTodayOrders waitingTransactions) {
        this.waitingTransactions = waitingTransactions;
    }

    public ListTodayOrders getAccomplishedTransactions() {
        return accomplishedTransactions;
    }

    public void setAccomplishedTransactions(ListTodayOrders accomplishedTransactions) {
        this.accomplishedTransactions = accomplishedTransactions;
    }

    //Generates transfer from FTlist orders

    public void payFtTransfer(){
        for (int i = 0; i< accomplishedTransactions.size(); i++){
            for (int clientsCounter = 0; clientsCounter < Server.getClientDatabase().size(); clientsCounter++){
                for (int accountsCounter = 0; accountsCounter < Server.getClientDatabase().get(clientsCounter).getAccounts().size(); accountsCounter++){
                    if (Server.getClientDatabase().get(clientsCounter).getAccounts().get(accountsCounter).getAccountNumber().equals(accomplishedTransactions.get(i).getRecipientAccountNo())){
                        try {
                            transfer(accomplishedTransactions.get(i).getAmount(), "FtTransfer", Server.getClientDatabase().get(clientsCounter).getAccounts().get(accountsCounter), accomplishedTransactions.get(i).getName(),accomplishedTransactions.get(i).getDescription() );
                        }catch (NotEnoughMoneyException ne){
                            ListTodayOrders temp = TransfersFT.getListTodayOrders();
                            accomplishedTransactions.get(i).setApproved(false);
                            temp.add(accomplishedTransactions.get(i));
                            TransfersFT.setListTodayOrders(temp);
                        }catch (TransferValueException te){
                            ListTodayOrders temp = TransfersFT.getListTodayOrders();
                            accomplishedTransactions.get(i).setApproved(false);
                            temp.add(accomplishedTransactions.get(i));
                            TransfersFT.setListTodayOrders(temp);
                        }
                    }else{
                        try {
                            transfer(accomplishedTransactions.get(i).getAmount(), "FtTransfer", accomplishedTransactions.get(i).getRecipientAccountNo(), accomplishedTransactions.get(i).getName(),accomplishedTransactions.get(i).getDescription() );
                        }catch (NotEnoughMoneyException ne){
                            ListTodayOrders temp = TransfersFT.getListTodayOrders();
                            accomplishedTransactions.get(i).setApproved(false);
                            temp.add(accomplishedTransactions.get(i));
                            TransfersFT.setListTodayOrders(temp);
                        }catch (TransferValueException te){
                            ListTodayOrders temp = TransfersFT.getListTodayOrders();
                            accomplishedTransactions.get(i).setApproved(false);
                            temp.add(accomplishedTransactions.get(i));
                            TransfersFT.setListTodayOrders(temp);
                        }
                    }
                }
            }
        }
    }
}
