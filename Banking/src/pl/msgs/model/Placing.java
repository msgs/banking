package pl.msgs.model;

import java.io.Serializable;

/**
 * Account to saving money.
 */
public class Placing extends Account implements Serializable {

    private double bankRate = 0; //percent

    public Placing(Person owner, double bankRate) {
        super(owner);
        this.bankRate = bankRate;
    }


    public Placing(Person owner, double balance, double bankRate) {
        super(owner, balance);
        this.bankRate = bankRate;
    }

    /**
     * Calculate and adds interest to the account balance.
     */
    public void applyInterest() {
        double growthBalance = getBalance() * bankRate / 100;
        addValue(growthBalance);
    }

    public double getBankRate() {
        return bankRate;
    }

    public void setBankRate(int bankRate) {
        this.bankRate = bankRate;
    }
}
