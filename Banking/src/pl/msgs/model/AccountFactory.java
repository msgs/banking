package pl.msgs.model;

import pl.msgs.model.exception.AccountException;

public class AccountFactory {
    private static Account account;

    //This section generate account with balance = 0
    public static Account getAccountWithoutBalance (String type, Person person, double bankRate ) throws AccountException{
        switch(type){
            case "standard":
                account = new Account(person);
                break;
            case "placing":
                account = new Placing(person, bankRate);
                break;
            case "credit":
                account = new CreditAccount(person, bankRate);
                break;
            default:
                throw new AccountException("Incorrect data!");

        }
        return account;
    }
    //This section generate account with balanceOrCredit
    public static Account getAccountWithBalance (String type, Person person, double balanceOrCredit, double bankRate) throws AccountException{
        switch(type){
            case "standard":
                account = new Account(person, balanceOrCredit);
                break;
            case "placing":
                account = new Placing(person, balanceOrCredit, bankRate);
                break;
            case "credit":
                account = new CreditAccount(person, balanceOrCredit, bankRate);
                break;
            default:
                throw new AccountException("Incorrect data!");

        }
        return account;
    }
}
