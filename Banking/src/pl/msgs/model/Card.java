package pl.msgs.model;

import java.io.Serializable;

/**
 * The universal class for account cards.
 */
// We should prepare classes which will have detailed information about card:
// debit card - connected with account, credit card - connected with something like credit etc.
public class Card implements Serializable {
    // First card number in a bank database.
    private static final long firstCardNumber = 6040321500000000L;
    private static long nextCardNumber = firstCardNumber;

    private final long cardNumber;    // 16 digits.
    private final Person owner;
    private double cost; //conduct of account cost per month ?? cost of a card, some kind of limit or cash used by this card ??

    // Constructor of this class.
    public Card(Person owner, double cost) {
        this.owner = owner;
        this.cost = cost;
        this.cardNumber = nextCardNumber++;
    }

    // Getters and setters
    public long getCardNumber() {
        return cardNumber;
    }

    public Person getOwner() {
        return owner;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public static long getFirstCardNumber() {
        return firstCardNumber;
    }

    public static long getNextCardNumber() {
        return nextCardNumber;
    }


    // Main method to check functionality.
    public static void main(String[] args) {
        Card card = new Card(null, 10);

        System.out.println("Card owner: " + card.getOwner());
        System.out.println("Card number: " + card.getCardNumber());
        System.out.println("Card cost: " + card.getCost());
        card.setCost(12.5);
        System.out.println("New card cost: " + card.getCost());

        System.out.println("Next card number: " + Card.getNextCardNumber());

    }
}
