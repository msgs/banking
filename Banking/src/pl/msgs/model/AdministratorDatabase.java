package pl.msgs.model;

import java.io.*;
import java.util.ArrayList;

public class AdministratorDatabase extends ArrayList<Administrator> implements Serializable {

    /**
     * Checks if in the administrator database exist administrator with specific id.
     *
     * @param adminID Searched client id.
     * @return Administrator witch id equals to adminID or null if database does not contain this ID.
     */
    public Administrator getAdminByID(int adminID) {
        int iterator = 0;
        while (iterator < this.size()) {
            if (this.get(iterator).getId() == adminID) {
                return this.get(iterator);
            } else {
                iterator++;
            }
        }
        return null;
    }

    /**
     * Saves database to a file specified by path parameter.
     *
     * @param path path to a file.
     * @return true if saving finished successfully.
     */
    public boolean saveDataToFile(String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                if (file.getParentFile() != null)
                    file.getParentFile().mkdirs();  // Creates parent directories if not exists.
                file.createNewFile();
            }
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file, false));
            this.forEach(administrator -> {
                try {
                    out.writeObject(administrator);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Loads database from a file specified by path parameter.
     *
     * @param path path to a file.
     * @return true if loading finished successfully.
     */
    public boolean loadDataFromFile(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                ObjectInput in = new ObjectInputStream(new FileInputStream(file));
                Administrator temp;
                try {
                    while ((temp = (Administrator) in.readObject()) != null)
                        this.add(temp);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                in.close();
            }
        } catch (EOFException e) {
            // Just end of file.
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }



}
