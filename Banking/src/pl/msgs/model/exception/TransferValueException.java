package pl.msgs.model.exception;

/**
 * Class for generating exception threw by Account class.
 */
public class TransferValueException extends Throwable {
    public TransferValueException(String message) {
        super(message);
    }
}
