package pl.msgs.model.exception;

/**
 * Class for generating exception threw by Account class.
 */
public class NotEnoughMoneyException extends Exception {
    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
