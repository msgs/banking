package pl.msgs.model;

import pl.msgs.model.exception.AccountException;
import pl.msgs.model.exception.NotEnoughMoneyException;

import java.io.Serializable;

public class CreditAccount extends Account implements Serializable {
    private double bankRate = 0; // percent as a cost of a credit.
    private double credit;  // amount of credit to pay off.

    public CreditAccount(Person owner, double bankRate) {
        super(owner);
        this.bankRate = bankRate;
        this.credit = 0;
    }

    public CreditAccount(Person owner, double credit, double bankRate) {
        super(owner, credit);
        this.bankRate = bankRate;
        this.credit = credit;
    }

    /**
     * Pays off (subtracts credit amount) credit.
     * @param value subtracting form credit value.
     * @throws NotEnoughMoneyException when value is bigger than balance.
     * @throws AccountException when value is bigger than credit.
     */
    public void payOff(double value) throws NotEnoughMoneyException, AccountException {
        if (value > 0) {
            if (value > getBalance()) {
                throw new NotEnoughMoneyException("You do not have enough money to pay off");
            } else if (value > credit) {
                throw new AccountException("You cannot over pay off credit value");
            } else {
                subtractValue(value);
                credit -= value;
            }
        }
    }

    /**
     * Calculate and adds interest to the account credit.
     */
    public void applyInterest() {
        double countIntrest = credit * bankRate / 100;
        credit += countIntrest;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public double getBankRate() {
        return bankRate;
    }

    public void setBankRate(double bankRate) {
        this.bankRate = bankRate;
    }

}
