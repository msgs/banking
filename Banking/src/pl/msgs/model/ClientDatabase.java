package pl.msgs.model;

import java.io.*;
import java.util.ArrayList;

public class ClientDatabase extends ArrayList<Person> implements Serializable {

    /**
     * Checks if in the client database exist client with specific id.
     *
     * @param clientID Searched client id.
     * @return Person witch id equals to clientID or null if database does not contain this ID.
     */
    public Person getClientByID(int clientID) {
        int iterator = 0;
        while (iterator < this.size()) {
            if (this.get(iterator).getLoginData().getPersonID() == clientID) {
                return this.get(iterator);
            } else {
                iterator++;
            }
        }
        return null;
    }


    /**
     * Checks if in the client database exist client with specific personal ID number.
     *
     * @param personIDNumber Searched Personal ID Number
     * @return Person witch personal ID number equals to personIDNumber or null if database does not contain this personal ID number.
     */
    public Person getClientByPersonID(String personIDNumber) {
        int iterator = 0;
        while (iterator < this.size()) {
            //if (Long.parseLong(this.get(iterator).getPersonIDNumber()) == Long.parseLong(personIDNumber)) {
            if (personIDNumber.equalsIgnoreCase(this.get(iterator).getPersonIDNumber())) {
                return this.get(iterator);
            } else {
                iterator++;
            }
        }
        return null;
    }
    /**
     * Saves database to a file specified by path parameter.
     *
     * @param path path to a file.
     * @return true if saving finished successfully.
     */
    public boolean saveDataToFile(String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                if (file.getParentFile() != null)
                    file.getParentFile().mkdirs();  // Creates parent directories if not exists.
                file.createNewFile();
            }
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file, false));
            this.forEach(person -> {
                try {
                    out.writeObject(person);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Loads database from a file specified by path parameter.
     *
     * @param path path to a file.
     * @return true if loading finished successfully.
     */
    public boolean loadDataFromFile(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                ObjectInput in = new ObjectInputStream(new FileInputStream(file));
                Person temp;
                try {
                    while ((temp = (Person) in.readObject()) != null)
                        this.add(temp);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                in.close();
            }
        } catch (EOFException e) {
            // Just end of file.
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

}
