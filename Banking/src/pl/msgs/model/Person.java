package pl.msgs.model;

import pl.msgs.FTConnection.ListTodayOrders;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;

/**
 * A class that describes a single person/client in our bank.
 */
public class Person implements Serializable {
    private String firstName;
    private String lastName;
    private LocalDate birthdayDate;
    private final Address homeAddress;
    private String phoneNumber;
    private ArrayList<Account> accounts;
    private String personIDNumber;
    private LoginData loginData;


    // Constructors of the class.
    public Person(String firstName, String lastName, int birthdayDay, int birthdayMonth, int birthdayYear, String street, String building, String flat, String zipCode, String city, String country, String phoneNumber, String personIDNumber, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdayDate = LocalDate.of(birthdayYear, birthdayMonth, birthdayDay);
        this.homeAddress = new Address(street, building, flat, zipCode, city, country);
        this.phoneNumber = phoneNumber;
        this.accounts = new ArrayList<Account>();
        this.personIDNumber = personIDNumber;
        this.loginData = new LoginData(password);
    }

    public Person(Person otherPerson) {
        this.firstName = otherPerson.firstName;
        this.lastName = otherPerson.lastName;
        this.birthdayDate = otherPerson.birthdayDate;
        this.homeAddress = otherPerson.homeAddress;
        this.phoneNumber = otherPerson.phoneNumber;
        this.accounts = new ArrayList<Account>();
        this.personIDNumber = otherPerson.personIDNumber;
        this.loginData = new LoginData(otherPerson.loginData.getPassword());
    }

    // Constructor of the class with asPrototype parameter.
    public Person(String firstName, String lastName, int birthdayDay, int birthdayMonth, int birthdayYear, String street, String building, String flat, String zipCode, String city, String country, String phoneNumber, String personIDNumber, String password, boolean asPrototype) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdayDate = LocalDate.of(birthdayYear, birthdayMonth, birthdayDay);
        this.homeAddress = new Address(street, building, flat, zipCode, city, country);
        this.phoneNumber = phoneNumber;
        this.accounts = new ArrayList<Account>();
        this.personIDNumber = personIDNumber;
        this.loginData = new LoginData(password, asPrototype);
    }

    public Person(Person otherPerson, boolean asPrototype) {
        this.firstName = otherPerson.firstName;
        this.lastName = otherPerson.lastName;
        this.birthdayDate = otherPerson.birthdayDate;
        this.homeAddress = otherPerson.homeAddress;
        this.phoneNumber = otherPerson.phoneNumber;
        this.accounts = new ArrayList<Account>();
        this.personIDNumber = otherPerson.personIDNumber;
        this.loginData = new LoginData(otherPerson.loginData.getPassword(), asPrototype);
    }

    /**
     * Updates client data info.
     * @param otherPerson
     */
    public void updatePerson(Person otherPerson) {
        this.firstName = otherPerson.firstName;
        this.lastName = otherPerson.lastName;
        this.birthdayDate = otherPerson.birthdayDate;
        this.homeAddress.setStreet(otherPerson.homeAddress.getStreet());
        this.homeAddress.setBuilding(otherPerson.homeAddress.getBuilding());
        this.homeAddress.setFlat(otherPerson.homeAddress.getFlat());
        this.homeAddress.setZipCode(otherPerson.homeAddress.getFlat());
        this.homeAddress.setCity(otherPerson.homeAddress.getCity());
        this.homeAddress.setCountry(otherPerson.homeAddress.getCountry());
        this.phoneNumber = otherPerson.phoneNumber;
        this.personIDNumber = otherPerson.personIDNumber;
        this.loginData = new LoginData(otherPerson.loginData.getPassword());
    }

    /**
     * Code a person to String for further transmission.
     *
     * @return String coded person.
     * @throws IOException
     */
    public String personToCodedString() throws IOException {
        String personInString;
        ByteArrayOutputStream baout = new ByteArrayOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(baout);
        oout.writeObject(this);
        personInString = Base64.getEncoder().encodeToString(baout.toByteArray());
        oout.close();
        return personInString;
    }

    /**
     * Gets a person from a received string.
     *
     * @param personString
     * @return Person
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Person getPersonFromCodedString(String personString) throws IOException, ClassNotFoundException {
        Person temp;

        byte[] data = Base64.getDecoder().decode(personString);
        ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(data));
        temp = (Person) oin.readObject();
        oin.close();
        return temp;
    }

    public String addAccount (Account account){
        accounts.add(account);
        return account.getAccountNumber();
    }

    // Getters and setters.
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public String getPersonIDNumber() {
        return personIDNumber;
    }

    public void setPersonIDNumber(String personIDNumber) {
        this.personIDNumber = personIDNumber;
    }

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }


}
