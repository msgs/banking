package pl.msgs.model;

import java.io.Serializable;

/**
 * A class that contains the details about an address.
 */
public class Address implements Serializable {
    private String street;
    private String building;
    private String flat;    //if no flat, flat = null
    private String zipCode;
    private String city;
    private String country;

    // Constructor of the class.
    public Address(String street, String building, String flat, String zipCode, String city, String country) {
        this.street = street;
        this.building = building;
        this.flat = flat;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }

    // Getters and setters.
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
