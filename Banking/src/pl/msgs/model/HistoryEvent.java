package pl.msgs.model;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Class that describes individual event in account history connected with cash flow.
 */
public class HistoryEvent implements Serializable {
    private String type;    // transfer or card
    private LocalDate date;
    private double value;
    private String title;
    private String comment;

    // Constructor of this class.
    public HistoryEvent(String type, LocalDate date, double value, String title, String comment) {
        this.type = type;
        this.date = date;
        this.value = value;
        this.title = title;
        this.comment = comment;
    }

    // Setters
    public String getType() {
        return type;
    }

    public LocalDate getDate() {
        return date;
    }

    public double getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    public String getComment() {
        return comment;
    }
}
