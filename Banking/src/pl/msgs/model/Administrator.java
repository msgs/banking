package pl.msgs.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

/**
 * A class that describes a single administrator in our bank.
 */
public class Administrator implements Serializable {
    // First administrator ID number in a bank database.
    private static final int firstAdminID = 147258789;
    private static int nextAdminID = firstAdminID;

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private int id;
    private String password;

    // Constructor of this class.
    public Administrator(String firstName, String lastName, String phoneNumber, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.id = nextAdminID++;
        this.password = password;

    }

    // Getters and setters
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    /**
     * Generates seven indexes of the password to check if it is correct.
     *
     * @return int[] a five elements array.
     */
    public int[] getIndexesToCheck() {
        int[] indexes = new int[7];
        Random random = new Random();
        int temp;
        boolean test;

        // Get five indexes to indexes array.
        for (int i = 0; i < indexes.length; i++) {
            // Finds a unique index.
            do {
                test = false;
                temp = random.nextInt(password.length());

                // Checks if temp is unique.
                for (int j = 0; j < i && !test; j++) {
                    if (indexes[j] == temp)
                        test = true;
                }
            } while (test);
            indexes[i] = temp;
        }
        Arrays.sort(indexes);

        return indexes;
    }

    /**
     * Checks if symbols of password match with password.
     *
     * @param indexes array of int. Indexes of password.
     * @param symbols array of char. Chars at corresponding indexes.
     * @return true if symbols of password match with password return true.
     */
    public boolean checkAdminPasswordByIndexes(int[] indexes, char[] symbols) {
        boolean test = true;
        if (indexes.length == symbols.length) {
            for (int i = 0; i < indexes.length && test; i++) {
                if (password.charAt(indexes[i]) != symbols[i]) {
                    test = false;
                }
            }
            // If symbols of password match with password return true.
            if (test)
                return true;
        }
        return false;
    }

    /**
     * Checks whether two administrators are equals.
     *
     * @param otherAdmin
     * @return true when they are the same administrator.
     */
    public boolean equals(Administrator otherAdmin) {
        if (this.firstName.equals(otherAdmin.firstName) && this.lastName.equals(otherAdmin.lastName) && this.id == otherAdmin.id)
            return true;
        return false;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
