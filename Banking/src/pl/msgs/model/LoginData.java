package pl.msgs.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

/**
 * The class to store and manage sensitive login data to bank accounts.
 */
// Some coding of those data may be useful.
public class LoginData implements Serializable {
    // First person ID number in a bank database.
    private static final int firstPersonID = 20034100;
    private static int nextPersonID = firstPersonID;

    private int personID;
    private String password;

    /**
     * Constructor of this class.
     * @param password which will be a person new password to online application. Must be longer than 4 symbols.
     */
    public LoginData(String password) {
        this.personID = nextPersonID++;
        this.password = password;
    }

    /**
     * Constructor of this class.
     * @param password which will be a person new password to online application. Must be longer than 4 symbols.
     */
    public LoginData(String password, boolean asPrototype) {
        if(asPrototype) {
            this.personID = 0;
        }
        this.password = password;
    }

    /**
     * Checks if the person ID is correct.
     * @param personID int as an ID.
     * @return true if person ID is correct.
     */
    public boolean checkPersonID(int personID) {
        if (personID == this.personID)
            return true;
        else
            return false;
    }

    /**
     * Checks if the person password is correct.
     * @param password String to check.
     * @return true if password is correct.
     */
    public boolean checkPersonPassword(int password) {
        if (this.password.equals(password))
            return true;
        else
            return false;
    }

    /**
     * Generates five indexes of the password to check if it is correct.
     * @return int[] a five elements array.
     */
    public int[] getIndexesToCheck() {
        int[] indexes = new int[5];
        Random random = new Random();
        int temp;
        boolean test;

        // Get five indexes to indexes array.
        for (int i = 0; i < indexes.length; i++) {
            // Finds a unique index.
            do {
                test = false;
                temp = random.nextInt(password.length());

                // Checks if temp is unique.
                for (int j = 0; j < i && !test; j++) {
                    if (indexes[j] == temp)
                        test = true;
                }
            } while (test);
            indexes[i] = temp;
        }
        Arrays.sort(indexes);

        return indexes;
    }

    /**
     * Checks if symbols of password match with password.
     * @param indexes array of int. Indexes of password.
     * @param symbols array of char. Chars at corresponding indexes.
     * @return true if symbols of password match with password return true.
     */
    public boolean checkPersonPasswordByIndexes(int[] indexes, char[] symbols) {
        boolean test = true;
        if (indexes.length == symbols.length) {
            for (int i = 0; i < indexes.length && test; i++) {
                if (password.charAt(indexes[i]) != symbols[i]){
                    test = false;
                }
            }
            // If symbols of password match with password return true.
            if (test)
                return true;
        }
        return false;
    }

    // Getters and setters
    public int getPersonID() {
        return personID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static int getNextPersonID() {
        return nextPersonID;
    }

    public static int getFirstPersonID() {
        return firstPersonID;
    }
}
