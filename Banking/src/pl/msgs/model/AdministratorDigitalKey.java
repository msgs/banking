package pl.msgs.model;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Base64;
import java.util.Scanner;

/**
 * Class defining simply blockchain digital key to authorization;
 */
public class AdministratorDigitalKey implements Serializable {
    private static final String firstHash = "#mt4A_DhtZwJIcgRScXD1s8Rfkgp2brCSm9ck-2&x035wtewt0nqfaHp-vj&IqP%3m)Q^F*DTJ$C3D)wP_TH1cmb8OdE!u8iQKj^Ou&TuN*vkwJ)yDkFqpv3KVMXK$N_gK8tnUzkGk^bxKCutBAV@k(7oxbH98Do#^BHlbzDV3VM)VCBuyS6LXE0Yy$HCTBRCihIwezMkICE#-(QF_eV*e-jWG7zMwcx)zrMNCSpUw0AgP_c8gEW4^okxyBuc#6A";
    private static String newestHash = firstHash;
    private static String pathToFolder = null;

    private Administrator admin;
    private LocalDate creationDate;
    private LocalTime creationTime;
    private String serverSessionStartTime;
    private String previousHash;

    /**
     * Specifies the path of all keys. Must be called before creating other digital keys.
     * @param aPathToFolder
     */
    public static void setPathToFolder(String aPathToFolder) {
        if (pathToFolder == null)
            pathToFolder = aPathToFolder;
    }

    public AdministratorDigitalKey(Administrator admin, LocalDate creationDate, LocalTime creationTime, String serverSessionStartTime) throws IOException {
        this.previousHash = newestHash;
        this.admin = admin;
        this.creationDate = creationDate;
        this.creationTime = creationTime;
        this.serverSessionStartTime = serverSessionStartTime;

        ByteArrayOutputStream baout = new ByteArrayOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(baout);
        oout.writeObject(this);
        newestHash = Base64.getEncoder().encodeToString(baout.toByteArray());
        oout.close();


        saveKeyToFile(pathToFolder);
    }

    public boolean validateKey(AdministratorDigitalKey otherKey) {
        if (firstHash.equals(otherKey.getFirstHash()) && this.previousHash.equals(otherKey.previousHash) && this.admin.equals(otherKey.admin) && this.creationDate.isEqual(otherKey.creationDate) && this.creationTime.equals(otherKey.creationTime) && this.serverSessionStartTime.equals(otherKey.serverSessionStartTime))
            return true;
        return false;
    }

    /**
     * Saves a key to a file in a folder specified by path parameter.
     *
     * @param pathToFolder path to a folder.
     * @return true if saving finished successfully.
     */
    private boolean saveKeyToFile(String pathToFolder) throws IOException {
        File file = new File(pathToFolder + "/" + admin.getId());
        if (!file.exists()) {
            if (file.getParentFile() != null)
                file.getParentFile().mkdirs();  // Creates parent directories if not exists.
            file.createNewFile();
        }
        String keyString;
        ByteArrayOutputStream baout = new ByteArrayOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(baout);
        oout.writeObject(this);
        keyString = Base64.getEncoder().encodeToString(baout.toByteArray());
        oout.close();

        PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file, false), StandardCharsets.UTF_16), true);
        out.println(keyString);
        out.close();
        return true;
    }

    /**
     * Gets a administrator key from a file in a folder specified by pathToFolder.
     *
     * @param admin
     * @return AdministratorDigitalKey
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static AdministratorDigitalKey getAdminKey(Administrator admin) throws IOException, ClassNotFoundException {
        File file = new File(pathToFolder + "/" + admin.getId());
        String keyString;
        Scanner in = new Scanner(new FileInputStream(file), "UTF-16");
        keyString = in.nextLine();
        in.close();
        AdministratorDigitalKey temp;

        byte[] data = Base64.getDecoder().decode(keyString);
        ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(data));
        temp = (AdministratorDigitalKey) oin.readObject();
        oin.close();
        return temp;
    }

    /**
     * Gets a administrator key from a file.
     *
     * @param keyFile
     * @return String key as a string.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static String getStringAdminKeyFromFile(File keyFile) throws IOException, ClassNotFoundException {
        String keyString;
        Scanner in = new Scanner(new FileInputStream(keyFile), "UTF-16");
        keyString = in.nextLine();
        in.close();
        return keyString;
    }

    /**
     * Gets a administrator key from a string.
     *
     * @param keyString
     * @return AdministratorDigitalKey
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static AdministratorDigitalKey getAdminKeyFromString(String keyString) throws IOException, ClassNotFoundException {
        AdministratorDigitalKey temp;

        byte[] data = Base64.getDecoder().decode(keyString);
        ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(data));
        temp = (AdministratorDigitalKey) oin.readObject();
        oin.close();
        return temp;
    }


    // Getter
    public String getFirstHash() {
        return firstHash;
    }
}
