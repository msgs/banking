package pl.msgs.FTConnection;

/**
 * Temp cleaner is used for cleaning temporary values
 *
 * Thread of this class is cleaning all temporary values once a day. Is is necessary to eliminates doubled transfers.
 * If some transfer it should be realize every day it must not be in in temporary memory from last day.
 */


public class TempCleaner implements Runnable {

    private Thread thread;
    private FTServices ftServices;
    private boolean running = true;


    //constructor
    public TempCleaner(FTServices ftServices) {
        this.ftServices = ftServices;
    }

    public void start(){
        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        running = false;
    }


    public void run(){
        while (running) {
            ftServices.setTemp(new ListTodayOrders());
            try {
                Thread.sleep(86400000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }

    }
}
