package pl.msgs.FTConnection;



import pl.msgs.server.Server;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * It is managing connection and communication with FT system.
 */

public class HandlingFT {

    private Socket socket;

    private static InputStream inStream;
    private static OutputStream outStream;
    private static Scanner in;
    private static PrintWriter out;
    private static final String READY = "ready";
    private static final String ERROR = "error";
    private static final String SENDING = "sending";
    private static final String CONFIRM = "confirm";
    private static final String END = "end";
    private Logger logger = Logger.getLogger(HandlingFT.class.getName());

    public HandlingFT(Socket socket) {
        this.socket = socket;
    }

    public void startConnection(){

        try {

            inStream = socket.getInputStream();
            outStream = socket.getOutputStream();
            in = new Scanner(inStream, "UTF-16");
            out = new PrintWriter(new OutputStreamWriter(outStream, StandardCharsets.UTF_16), true);

            String input, output;
            //waiting for orders
            out.println(READY);
            input = in.nextLine();
            //receiving orders
            while (!input.equals(CONFIRM) && !input.equals(ERROR)){
                ListTodayOrders tempList = TransfersFT.getListTodayOrders();
                String[] temp = input.split("/");
                Boolean approved;
                if (temp[7].equals("true")){
                    approved =true;
                }else{
                    approved = false;
                }
                ReadyTransfer tempTransfer = new ReadyTransfer(temp[0], temp[1], temp[2], temp[3],temp[4],Integer.parseInt(temp[5]),Integer.parseInt(temp[6]),approved,Double.parseDouble(temp[8]));
                tempList.add(tempTransfer);
                Server.setOrdersList(tempList);
                output = CONFIRM;
                out.println(output);
                input=in.nextLine();
            }
            //sending confirmations from clients
            ListTodayOrders todayOrders = TransfersFT.getListConfirmations();
            if (todayOrders.size() > 0) {
                for (int i = 0; i < todayOrders.size(); i++) {
                    output = todayOrders.get(i).toString();
                    out.println(output);
                    input = in.nextLine();
                    if (!input.equals(CONFIRM)){
                        output = ERROR;
                        out.println(output);
                        break;
                    }
                }
                output = CONFIRM;
                out.println(output);
            }else{
                output = ERROR;
                out.println(output);
            }
            input = in.nextLine();
            if (input.equals(END)){
                logger.info("Transmission OK");
            }


            in.close();
            out.close();


        }catch(NoSuchElementException e){
            //bankConnection.closeBankConnection();
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
