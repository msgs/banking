package pl.msgs.FTConnection;


import pl.msgs.server.Server;

/**
 * Services Transactions from FT system
 */


public class FTServices implements Runnable{

    private volatile boolean running = true;
    private volatile int time;
    private volatile ListTodayOrders temp = new ListTodayOrders();
    private Thread thread;
    private TempCleaner tempCleaner;

    public FTServices(boolean running, int time) {
        this.running = running;
        this.time = time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void endConnection(){
        tempCleaner.stop();
        running = false;
    }


    public void connect(){
        thread = new Thread(this);
        thread.start();
    }

    public void run() {
        FTConnection ftConnection = new FTConnection();
        tempCleaner = new TempCleaner(this);
        tempCleaner.start();
        while (running){
            ftConnection.startConnection();
            for (int i = 0; i < TransfersFT.getListTodayOrders().size(); i++){
                if (!temp.contains(TransfersFT.getListTodayOrders().get(i)) && !containsListConfirmations(TransfersFT.getListTodayOrders().get(i))){
                    temp.add(TransfersFT.getListTodayOrders().get(i));
                }
            }
            Server.setOrdersList(temp);
            try {
                Thread.sleep(time);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }


    //eliminates doubled orders
    private boolean containsListConfirmations(ReadyTransfer transfer){
        int k = 0;
        for (int i = 0; i < TransfersFT.getListConfirmations().size(); i++){
            if (transfer.getId() != TransfersFT.getListConfirmations().get(i).getId()){
                k++;
            }
        }
        if (k == TransfersFT.getListConfirmations().size()){
            return false;
        }else{
            return true;
        }
    }

    //getters and setters
    public ListTodayOrders getTemp() {
        return temp;
    }

    public void setTemp(ListTodayOrders temp) {
        this.temp = temp;
    }
}
