package pl.msgs.FTConnection;


import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Class witch is used to connect bank with FT server
 */


public class FTConnection implements Runnable {

    private final int port = 7777;
    private final String host = "localhost";
    private Thread thread;
    private Logger logger = Logger.getLogger(FTConnection.class.getName());




    public void startConnection(){
        thread = new Thread(this);
        thread.start();
    }



    public void run(){
        try {

            Socket socket = new Socket(host, port);//((SSLSocketFactory) SSLSocketFactory.getDefault()).createSocket(host, port); //

            logger.info("Connection with FT has been made.");
            HandlingFT handlingFT = new HandlingFT(socket);
            handlingFT.startConnection();

            socket.close();
            logger.info("End of FT connection.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
