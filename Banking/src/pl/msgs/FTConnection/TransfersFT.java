package pl.msgs.FTConnection;


import pl.msgs.model.ClientDatabase;
import pl.msgs.server.Server;

/**
 * This is temporary memory of data form FT system
 *
 * It is used ro manage data which is receives and sends to Ft system.
 */

public class TransfersFT {

    private static volatile ListTodayOrders listTodayOrders = new ListTodayOrders();
    private static volatile ListTodayOrders listConfirmations = new ListTodayOrders();


    //getters and setters
    public static ListTodayOrders getListTodayOrders() {
        return listTodayOrders;
    }

    public static void setListTodayOrders(ListTodayOrders listTodayOrders) {
        TransfersFT.listTodayOrders = listTodayOrders;
    }

    public static ListTodayOrders getListConfirmations() {
        ClientDatabase clientDatabase = Server.getClientDatabase();
        for (int i =0; i<clientDatabase.size(); i++){
            for (int j= 0; j < clientDatabase.get(i).getAccounts().size(); j++){
                for (int k = 0; k < clientDatabase.get(i).getAccounts().get(j).getAccomplishedTransactions().size(); k++){
                    listConfirmations.add(clientDatabase.get(i).getAccounts().get(j).getAccomplishedTransactions().get(k));
                }
            }
        }
        return listConfirmations;
    }

    public static void setListConfirmations(ListTodayOrders listConfirmations) {
        TransfersFT.listConfirmations = listConfirmations;
    }
}
