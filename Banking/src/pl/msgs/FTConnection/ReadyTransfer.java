package pl.msgs.FTConnection;

import java.io.Serializable;

/**
 * Ready transfer is a transfer which is ready to realize in the bank.
 */

public class ReadyTransfer implements Serializable {

    private double amount;
    private String date;
    private String description;
    private int userId;
    private int id;
    private boolean approved;

    private String name;
    private String payerAccountNo;
    private String recipientAccountNo;

    //constructors
    public ReadyTransfer(String date, String description, String name, String payerAccountNo, String recipientAccountNo, int userId, int id, boolean approved, double amount) {
        this.date = date;
        this.description = description;
        this.name = name;
        this.payerAccountNo = payerAccountNo;
        this.recipientAccountNo = recipientAccountNo;
        this.userId = userId;
        this.id = id;
        this.approved = approved;
        this.amount = amount;
    }

    public ReadyTransfer(Transfer transfer, double amount) {
        this.date = transfer.getDate();
        this.description = transfer.getDescription();
        this.name = transfer.getName();
        this.payerAccountNo = transfer.getPayerAccountNo();
        this.recipientAccountNo = transfer.getRecipientAccountNo();
        this.userId = transfer.getUserId();
        this.id = transfer.getId();
        this.approved = transfer.getApproved();
        this.amount = amount;

    }

    public ReadyTransfer() {
    }

    @Override
    public String toString(){
        if (approved) {
            return "" + getDate() + "/" + getDescription() + "/" + getName() + "/" + getPayerAccountNo() + "/" + getRecipientAccountNo() + "/" + getUserId() + "/" + getId() + "/true/" + getAmount();
        }else {
            return "" + getDate() + "/" + getDescription() + "/" + getName() + "/" + getPayerAccountNo() + "/" + getRecipientAccountNo() + "/" + getUserId() + "/" + getId() + "/false/" + getAmount();
        }
    }

    //getters and setters


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayerAccountNo() {
        return payerAccountNo;
    }

    public void setPayerAccountNo(String payerAccountNo) {
        this.payerAccountNo = payerAccountNo;
    }

    public String getRecipientAccountNo() {
        return recipientAccountNo;
    }

    public void setRecipientAccountNo(String recipientAccountNo) {
        this.recipientAccountNo = recipientAccountNo;
    }
}
