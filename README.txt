#  README  #

English

In this repository is Online Banking System project which is creating by Mikolaj Gwiazdowicz and Szymon Stryczek.

Duplication this project without owner's approval is prohibited.

Using whole of this code or extract of it to other projects without owner's approval is prohibited.

All rights to this project belong to Mikolaj Gwiazdowicz and Szymon Stryczek.

Owners:

Mikolaj Gwiazdowicz
mikolaj.rx8@gmail.com

Szymon Stryczek
szymon.str@onet.eu


Polish/Polski

W tym repozytorium znajduje si� System Bankowy Internetowej, tworzony przez Miko�aja Gwiazdowicza i Szymona Stryczka.

Kopiowanie projektu bez zgody w�a�ciciela jest zabronione.

Wykorzystywanie ca�o�ci kodu lub jego fragmentu do innych projekt�w bez zgody w�a�ciciela jest zabronione.

Wszelkie prawa do tego projektu nale�� do Miko�aja Gwiazdowicza i Szymona Stryczka.

W�a�ciciele:

Miko�aj Gwiazdowicz
mikolaj.rx8@gmail.com

Szymon Stryczek
szymon.str@onet.eu